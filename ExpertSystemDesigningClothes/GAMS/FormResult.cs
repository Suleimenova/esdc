﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemDesigningClothes.GAMS
{
    public partial class FormResult : Form
    {
        List<Panel_ListImage> panelListImage;
        int[,] matrix_ID;
        String[,] matrix;
        FormGAMS formGAMS;

        public FormResult(String[,] matrix, int[,] matrix_ID, List<Panel_ListImage> panelListImage, FormGAMS formGAMS)
        {
            InitializeComponent();

            this.panelListImage = panelListImage;
            this.matrix_ID = matrix_ID;
            this.matrix = matrix;
            this.formGAMS = formGAMS;

            CreateResultImage();
        }

        private void CreateResultImage()
        {
            List<Image> files = new List<Image>();
            for (int i = 0; i < matrix.GetLength(1); i++)
                for (int j = 0; j < matrix.GetLength(0); j++)
                {
                    foreach (Panel_ListImage panel in panelListImage)
                    {
                        if (matrix[j,i] == "1" && matrix_ID[j, i] == panel.ID)
                            files.Add(panel.image);
                        pictureBox.Image = panel.image;
                    }
                }
            pictureBox.Image = Program.formMain.combineImageHard(files);
        }

        private void saveImageButton_Click(object sender, EventArgs e)
        {
            if (saveImageDialog.ShowDialog() == DialogResult.OK)
                pictureBox.Image.Save(saveImageDialog.FileName);
        }

        private void openFormMainButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < matrix.GetLength(1); i++)
                for (int j = 0; j < matrix.GetLength(0); j++)
                {
                    foreach (Panel_ListImage panel in panelListImage)
                    {
                        if (matrix[j, i] == "1" && matrix_ID[j, i] == panel.ID)
                        {
                            panel.flagItemIsActive = true;
                            panel.weight = 1;
                        }
                        else
                        {
                            panel.flagItemIsSelected = false;
                        }
                    }
                }
            Program.formMain.panelListImage = panelListImage;
            Program.formMain.combineImageLight();
            Program.formMain.addActiveElements();
            Program.formMain.tabControl.SelectTab(1);
            formGAMS.Close();
            this.Close();
        }
    }
}
