﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemDesigningClothes.GAMS
{
    public partial class FormGAMS : Form
    {
        List<Panel_ListImage> panelListImage;
        List<NumericSoftConstraint> softConstraints;
        String path;
        int[,] matrix_ID;   //Матрица элементов, для показа изображений элементов
        String[,] matrix;   //матрица с результатом расчета
        int rows = 0,       //rows: количество групп, 
            columns = 0;    //columns: количество элементов в группе
        int p;              //ограничение проектировщика
        int numberSoftConstraints = 0;

        public FormGAMS(List<Panel_ListImage> panelListImage)
        {
            InitializeComponent();

            this.panelListImage = panelListImage;
            softConstraints = new List<NumericSoftConstraint>();

            CreateListSoftConstraints();
            CreateMatrix();
        }
        //Cоздание списка мягких логических ограничений
        private void CreateListSoftConstraints() 
        {            
            ESDC_DataBaseDataSet.ConstraintsDataTable table = new ESDC_DataBaseDataSet.ConstraintsDataTable();
            constraintsTableAdapter.Fill(table);

            string subString = "z(\'1\',\'";

            foreach (DataRow row in table.Rows)
            {                
                int indexStart = row["Constraint"].ToString().IndexOf(subString);
                if (indexStart >= 0)
                {
                    string text = row["Constraint"].ToString().Substring(indexStart);
                    int indexEnd = row["Constraint"].ToString().IndexOf("\')");
                    text = text.Remove(indexEnd + 2);

                    numberSoftConstraints++;

                    NumericSoftConstraint numericSC = new NumericSoftConstraint();
                    numericSC.label.Text = text;
                    softConstraints.Add(numericSC);
                }
            }

            if (numberSoftConstraints == 0)
            {
                labelStatus.Text = "База знаний повреждена. Подробнее в журнале событий.\n";
                textBoxLog.Text += "\n\r";
                textBoxLog.Text += "\n\r" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + " "
                            + "Не удалось найти значения мягких ограничений. База знаний повреждена.\n";
            }
            foreach (NumericSoftConstraint constraint in softConstraints)
            {
                flowLayoutPanel.Controls.Add(constraint);
            }

            CreateMatrixSoftConstraints();
        }

        private String CreateMatrixSoftConstraints()
        {
            string matrixSoftConstraints = "";

            for (int i = 1; i <= numberSoftConstraints; i++)
            {
                matrixSoftConstraints += "	" + i;
            }
            matrixSoftConstraints += "\n" + 1;
            foreach (NumericSoftConstraint constraint in softConstraints)
            {
                matrixSoftConstraints += "	" + constraint.numericUpDown.Value;
            }            
            
            matrixSoftConstraints += "\n" + ";";

            Console.Write(matrixSoftConstraints);

                return matrixSoftConstraints;
        }
        
        private void CreateMatrix() 
        {
            ESDC_DataBaseDataSet.GroupsDataTable table = new ESDC_DataBaseDataSet.GroupsDataTable();
            groupsTableAdapter.Fill(table);
            rows = table.Rows.Count;

            //Алгоритм ужасен
            //Записываем все веса элементов в datagridview
            //Так как элементы хранятся в списке, то приходится делать сортировку.          
            
            int[] massrows = new int[rows];

            foreach (Panel_ListImage panel in panelListImage)
            {//Ищем количество элементов в каждой группе
                massrows[panel.ID_Group - 1] += 1;
                if (massrows[panel.ID_Group - 1] > columns)
                    columns = massrows[panel.ID_Group - 1];
            }

            dataGridView.ColumnCount = columns;
            dataGridView.RowCount = rows;

            matrix_ID = new int[columns, rows];
            
            //Название групп элементов
            int k = 0;
            foreach (DataRow row in table.Rows)
            {
                if (row["Type"].ToString() == "Elements  ")
                {
                    if (row["Name"].ToString() == "Поперечное членение стана")
                        dataGridView.Rows[k].HeaderCell.Value = "Поп.член.стана";
                    else if (row["Name"].ToString() == "Продольное членение стана")
                        dataGridView.Rows[k].HeaderCell.Value = "Прод.член.стана";
                    else if (row["Name"].ToString() == "Дополнительные составляющие")
                        dataGridView.Rows[k].HeaderCell.Value = "Доп.составляющие";
                    else
                        dataGridView.Rows[k].HeaderCell.Value = row["Name"].ToString();
                    k++;
                }
                if (k > rows - 1)
                    break;
            }


            //Костыль
            for (int i = 0; i < rows; i++)
                for (int j = 0; j < columns; j++)
                {
                    dataGridView[j, i].Style.BackColor = Color.LightGray;
                    //dataGridView[j, i].Value = 0;
                }

            foreach (Panel_ListImage panel in panelListImage)
            {
                for (int i = 0; i < rows; i++)
                    if (panel.ID_Group == i + 1)
                        for (int j = 0; j < columns; j++)
                            if (dataGridView[j, i].Value == null)
                            {
                                matrix_ID[j, i] = panel.ID;

                                dataGridView[j, i].Value = panel.weight;
                                if(panel.weight !=0) 
                                    dataGridView[j, i].Style.BackColor = Color.LightSkyBlue;
                                else 
                                    dataGridView[j, i].Style.BackColor = Color.LightGray;
                                break;
                                //Console.WriteLine("Элемент " + panel.ID + " был добавлен!");
                            }
            }
        }

        //Проверка на изменение файла с результатом
        private void CheckFileChanged(DateTime lastChanged, String path, int limit) 
        {
            DateTime currentChanged = System.IO.File.GetLastWriteTime(path);

            progressBar.Value = limit;

            if (lastChanged == currentChanged && limit < 100)
            {
                limit++;

                Thread.Sleep(100);

                CheckFileChanged(lastChanged, path, limit);
            }
            else
            {
                progressBar.Value = 100;
                textBoxLog.Text += "\n\r";
                textBoxLog.Text += "\n\r" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + " " 
                                + "Файл с результатом создан!" + "\n\r";
                textBoxLog.Text += "\n\r" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + " " 
                                + "CheckFileChanged: last = " + lastChanged + "; current = " + currentChanged + "\n\r";
                this.Cursor = Cursors.Arrow;

                buttonResult.Enabled = true;
            }
        }

        private void RunGams() 
        {
            try
            {
                path = "gamsproject.txt";
                DateTime lastChanged = System.IO.File.GetLastWriteTime(path);

                try
                {
                    CreateModelFile();

                    textBoxLog.Text += 
                    System.Diagnostics.Process.Start(Properties.Settings.Default.gamsDirectory, "gamsproject.gms"); //"d:\\IDE\\GAMS23.3\\gams.exe"               
                
                    try
                    {
                        //path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\gamsdir\\projdir\\ExpertSystemDesigningClothes.txt";

                        this.Cursor = Cursors.WaitCursor;

                        CheckFileChanged(lastChanged, path, 0);

                        String[] matrixFile = System.IO.File.ReadAllLines(path);

                        //!!!!! Очистка файла, разобраться с задержкой
                    

                        matrix = new String[columns, rows];
                        int h = 0;
                        bool flag = false; //Если true, то решение есть
                        for (int i = 0; i < rows; i++)
                        {
                            for (int j = 0; j < columns; j++)
                            {
                                if (matrixFile[h] == "1") flag = true;
                                matrix[j, i] = matrixFile[h];
                                h++;
                            }
                        }


                        ///////////////////////////////////////////////////////
                        //Вывод матрицы в текстбокс
                        //string f = "";
                        //for (int i = 0; i < rows; i++)
                        //{
                        //    f = "";
                        //    for (int j = 0; j < columns; j++)
                        //    {
                        //        f += @" " + matrix[j, i];
                        //    }

                        //    Console.WriteLine(f);
                        //}
                        ///////////////////////////////////////////////////////

                        if (flag)
                        {
                            for (int i = 0; i < rows; i++)
                                for (int j = 0; j < columns; j++)
                                    if (matrix[j, i] == "1")
                                    {
                                        dataGridView[j, i].Style.BackColor = Color.Lime;
                                    }

                            pictureBox.Image = Properties.Resources.Complete;
                            labelStatus.Text = "Решение найдено!\n";
                            labelStatus.TextAlign = ContentAlignment.MiddleLeft;
                        }
                        else
                        {
                            pictureBox.Image = Properties.Resources.Critical;
                            labelStatus.Text = "Решение не найдено! Попробуйте изменить матрицу или ограничение проектировщика.\n";
                        }
                    }
                    catch (Exception e)
                    {
                        pictureBox.Image = Properties.Resources.Critical;
                        labelStatus.Text = "Не удалось прочесть файл c результатом. Подробнее в журнале событий.\n";
                        textBoxLog.Text += "\n\r";
                        textBoxLog.Text += "\n\r" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + " " 
                                        + "Не удалось прочесть файл c результатом.\n" + e;
                    }
                }
                catch (Exception e)
                {
                    pictureBox.Image = Properties.Resources.Critical;
                    labelStatus.Text = "Не удалось запустить GAMS. Подробнее в журнале событий.\n";
                    textBoxLog.Text += "\n\r";
                    textBoxLog.Text += "\n\r" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + " " 
                                    + "Не удалось запустить GAMS.\n" + e;
                }
            }
            catch (Exception e)
            {
                pictureBox.Image = Properties.Resources.Critical;
                labelStatus.Text = "Не удалось найти файл gamsproject.txt. Подробнее в журнале событий.\n";
                textBoxLog.Text += "\n\r";
                textBoxLog.Text += "\n\r" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + " "
                                + " Не удалось найти файл gamsproject.txt \n" + e;
            }
        }

        //Cоздаем файл из GetModelText для открытия его в GAMS
        private void CreateModelFile() 
        {
            System.IO.FileInfo file = new System.IO.FileInfo("gamsproject.gms");
            System.IO.StreamWriter write_text;
            write_text = file.CreateText();         
            write_text.WriteLine(GetModelText());   //Записываем в файл текст
            write_text.Close();
        }

        //Создание матрицы элементов для GAMS
        private String CreateModelMatrix()
        {
            // 1 Поперечное членение стана               
            // 2 Продольное членение стана               
            // 3 Застежки                                
            // 4 Рукава                                  
            // 5 Карманы                                 
            // 6 Пояса                                   
            // 7 Воротник                                
            // 8 Дополнительные составляющие  
            //=l= меньше чем или равно;              
            //=g= больше чем или равно;              
            //=e= равно.  

            //Создаем матрицу для GAMS
            string matrixGAMS = "";
            for (int i = -1; i < dataGridView.RowCount; i++)
                for (int j = -1; j < dataGridView.ColumnCount; j++)
                {
                    if (i == -1 && (j + 2) != (dataGridView.ColumnCount + 1))
                    {
                        matrixGAMS += "	" + (j + 2);
                    }
                    else
                    {
                        if (i == -1) { i++; j = -1; matrixGAMS += "\n"; }
                        if (j == -1)
                            matrixGAMS += "" + (i + 1);
                        else
                            if (dataGridView[j, i].Value == null)
                                matrixGAMS += "	" + 0;
                            else
                                matrixGAMS += "	" + dataGridView[j, i].Value;
                        if (j == dataGridView.ColumnCount - 1)
                            matrixGAMS += "\n";
                    }
                }


           //Console.WriteLine(@"" + matrixGAMS);

            return matrixGAMS;
        }

        //Создание списка ограничений для GAMS
        private String CreateModelConstraint()
        {
            // 1 Поперечное членение стана               
            // 2 Продольное членение стана               
            // 3 Застежки                                
            // 4 Рукава                                  
            // 5 Карманы                                 
            // 6 Пояса                                   
            // 7 Воротник                                
            // 8 Дополнительные составляющие  
            //=l= меньше чем или равно;              
            //=g= больше чем или равно;              
            //=e= равно.  

            //Создаем список ограничений для GAMS
            string listConstraints = "";
            //Для соединения имен ограничений с самими ограничениями
            string temp = "";

            //Получаем таблицу с ограничениями
            ESDC_DataBaseDataSet.ConstraintsDataTable table = new ESDC_DataBaseDataSet.ConstraintsDataTable();
            constraintsTableAdapter.Fill(table);

            foreach (DataRow row in table.Rows)
            {
                listConstraints += "c_" + row[0].ToString() + "        " + row["Description"].ToString() + "\n";
                temp += "c_" + row[0].ToString() + "..        " + row["Constraint"].ToString() + ";\n";
            }

            listConstraints += "\n;\n\n" + temp;

            return listConstraints;
        }

        //Для получения модели проекта GAMS
        private String GetModelText()
        {
            String model = @"
$Title  Expert System Designing Clothes

Sets
k        amount       /1*" + numberSoftConstraints + @"/
o        _one         /1*1/

i        types        /1*" + rows + @"/
j        amount       /1*" + columns + @"/

Scalar p  constraint of designer /" + numericUpDown.Value + @"/ ;

Table d(o,k)  array of soft constraints" + "\n" + CreateMatrixSoftConstraints() + @"
Table s(i,j)  array of elements" + "\n" + CreateModelMatrix() + @"	;

Binary variable
z(o,k)   comment
y(i,j)   comment;
Free variable
pp       comment;

Equations" + "\n" + CreateModelConstraint() +
@"

MODEL ASSINGMENT /ALL/;
Display ASSINGMENT.resusd;
Solve ASSINGMENT using MIP maximizing pp;
Display p, s;
Display y.l, z.l;

file factors /gamsproject.txt/ ;
scalar col column number /1/;
scalar line column number /1/;
put factors ;
loop(i,
loop (j, put @col y.l(i,j):1:0/;);
col=1;
);
putclose factors;
";
            return model;
        }

        //Запускаем GAMS после нажатия на кнопку
        private void buttonRunGAMS_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < rows; i++)
                for (int j = 0; j < columns; j++)
                    if (Convert.ToInt32(dataGridView[j, i].Value) >= 1)
                        dataGridView[j, i].Style.BackColor = Color.LightSkyBlue;
                    else if (Convert.ToInt32(dataGridView[j, i].Value) == 0)
                        dataGridView[j, i].Style.BackColor = Color.LightGray;

            buttonResult.Enabled = false;
            RunGams();
        }

        //Показ изображения элемента
        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (Panel_ListImage panel in panelListImage)
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                    if (matrix_ID[e.ColumnIndex, e.RowIndex] == panel.ID)
                        pictureBoxElement.Image = panel.image;
            }
        }

        public String saveProject() {
            return GetModelText();
        }

        //Окно с изображением одежды, которую предложил GAMS
        private void buttonResult_Click(object sender, EventArgs e)
        {
            if (matrix != null)
            {
                FormResult result = new FormResult(matrix, matrix_ID, panelListImage, this);
                result.Show();
            }
            else 
            {
                textBoxLog.Text += "\n\r";
                textBoxLog.Text += "\n\r" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + " "
                                + " Не удалось открыть окно с результатом расчета. \n";
            }
        }
    }
}
