﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemDesigningClothes.OtherForms
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();

            gamsDirectory.Text = Properties.Settings.Default.gamsDirectory;
        }

        private void gamsDirectory_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Properties.Settings.Default.gamsDirectory = gamsDirectory.Text;
                Properties.Settings.Default.Save();
                statusLabel.Text = "Путь сохранен.";
                statusLabel.Image = Properties.Resources.StatusAnnotations_Complete_and_ok_16xLG_color;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.gamsDirectory = gamsDirectory.Text;
            Properties.Settings.Default.Save();
            statusLabel.Text = "Путь сохранен.";
            statusLabel.Image = Properties.Resources.StatusAnnotations_Complete_and_ok_16xLG_color;
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonOn.Checked)
            {
                Program.formMain.showBackground = true;
                Program.formMain.combineImageLight();
            }
            if (radioButtonOff.Checked)
            {
                Program.formMain.showBackground = false; 
                Program.formMain.combineImageLight();
            }
        }
    }
}
