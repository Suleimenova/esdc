﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemDesigningClothes.OtherForms
{
    public partial class Help_ESDC : Form
    {
        public Help_ESDC()
        {
            InitializeComponent();

            treeView.ExpandAll();
        }

        private void Help_ESDC_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "eSDC_DataBaseDataSet.Help". При необходимости она может быть перемещена или удалена.
            this.helpTableAdapter.Fill(this.eSDC_DataBaseDataSet.Help);

        }

        private string FindDescription(string name)
        {
            ESDC_DataBaseDataSet.HelpDataTable table = new ESDC_DataBaseDataSet.HelpDataTable();
            helpTableAdapter.Fill(table);
            string description = "";

            foreach (DataRow row in table.Rows)
            {               
                if (row["ID_Help"].ToString() == name)
                {
                    description = row["Description"].ToString();
                }
            }
            return description;
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {            
            richTextBox.Text = FindDescription(e.Node.Name);  
        }
    }
}
