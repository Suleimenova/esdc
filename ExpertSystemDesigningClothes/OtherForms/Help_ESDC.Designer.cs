﻿namespace ExpertSystemDesigningClothes.OtherForms
{
    partial class Help_ESDC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Введение");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Главное окно");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Расчет оптимального решения");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("«Просмотр справки» и  «О программе»");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Руководство пользователя", new System.Windows.Forms.TreeNode[] {
            treeNode2,
            treeNode3,
            treeNode4});
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Добавить/Изменить элемент");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Список элементов");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Добавить/Изменить ткань");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Список тканей");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Добавить/Изменить ограничение");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Список ограничений");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("Настройки");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("Окно «Редактор»", new System.Windows.Forms.TreeNode[] {
            treeNode6,
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode10,
            treeNode11,
            treeNode12});
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("Руководство эксперта", new System.Windows.Forms.TreeNode[] {
            treeNode13});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Help_ESDC));
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.treeView = new System.Windows.Forms.TreeView();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.eSDC_DataBaseDataSet = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSet();
            this.helpBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.helpTableAdapter = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.HelpTableAdapter();
            this.tableAdapterManager = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.TableAdapterManager();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eSDC_DataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helpBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBox
            // 
            this.richTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.richTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.richTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox.Location = new System.Drawing.Point(3, 3);
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.ReadOnly = true;
            this.richTextBox.Size = new System.Drawing.Size(433, 339);
            this.richTextBox.TabIndex = 0;
            this.richTextBox.Text = "Ошибка чтения данных =(";
            // 
            // treeView
            // 
            this.treeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeView.BackColor = System.Drawing.SystemColors.Control;
            this.treeView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeView.Location = new System.Drawing.Point(9, 12);
            this.treeView.Margin = new System.Windows.Forms.Padding(0);
            this.treeView.Name = "treeView";
            treeNode1.Name = "13";
            treeNode1.Text = "Введение";
            treeNode2.Name = "1";
            treeNode2.Text = "Главное окно";
            treeNode3.Name = "2";
            treeNode3.Text = "Расчет оптимального решения";
            treeNode4.Name = "3";
            treeNode4.Text = "«Просмотр справки» и  «О программе»";
            treeNode5.Checked = true;
            treeNode5.Name = "14";
            treeNode5.Text = "Руководство пользователя";
            treeNode6.Name = "5";
            treeNode6.Text = "Добавить/Изменить элемент";
            treeNode7.Name = "6";
            treeNode7.Text = "Список элементов";
            treeNode8.Name = "7";
            treeNode8.Text = "Добавить/Изменить ткань";
            treeNode9.Name = "8";
            treeNode9.Text = "Список тканей";
            treeNode10.Name = "9";
            treeNode10.Text = "Добавить/Изменить ограничение";
            treeNode11.Name = "10";
            treeNode11.Text = "Список ограничений";
            treeNode12.Name = "11";
            treeNode12.Text = "Настройки";
            treeNode13.Name = "4";
            treeNode13.Text = "Окно «Редактор»";
            treeNode14.Checked = true;
            treeNode14.Name = "15";
            treeNode14.Text = "Руководство эксперта";
            this.treeView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode5,
            treeNode14});
            this.treeView.ShowRootLines = false;
            this.treeView.Size = new System.Drawing.Size(209, 346);
            this.treeView.TabIndex = 1;
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BackColor = System.Drawing.SystemColors.Window;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(676, 371);
            this.splitContainer1.SplitterDistance = 218;
            this.splitContainer1.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.richTextBox);
            this.panel1.Location = new System.Drawing.Point(3, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(437, 347);
            this.panel1.TabIndex = 1;
            // 
            // eSDC_DataBaseDataSet
            // 
            this.eSDC_DataBaseDataSet.DataSetName = "ESDC_DataBaseDataSet";
            this.eSDC_DataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // helpBindingSource
            // 
            this.helpBindingSource.DataMember = "Help";
            this.helpBindingSource.DataSource = this.eSDC_DataBaseDataSet;
            // 
            // helpTableAdapter
            // 
            this.helpTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ClothsTableAdapter = null;
            this.tableAdapterManager.ColorScalesTableAdapter = null;
            this.tableAdapterManager.ColorsTableAdapter = null;
            this.tableAdapterManager.ConstraintsTableAdapter = null;
            this.tableAdapterManager.ElementsTableAdapter = null;
            this.tableAdapterManager.GroupsTableAdapter = null;
            this.tableAdapterManager.HelpTableAdapter = this.helpTableAdapter;
            this.tableAdapterManager.UpdateOrder = ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // Help_ESDC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 371);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(690, 410);
            this.Name = "Help_ESDC";
            this.Text = "Справка";
            this.Load += new System.EventHandler(this.Help_ESDC_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.eSDC_DataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helpBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private ESDC_DataBaseDataSet eSDC_DataBaseDataSet;
        private System.Windows.Forms.BindingSource helpBindingSource;
        private ESDC_DataBaseDataSetTableAdapters.HelpTableAdapter helpTableAdapter;
        private ESDC_DataBaseDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Panel panel1;
    }
}