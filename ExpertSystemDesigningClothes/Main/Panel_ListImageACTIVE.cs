﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemDesigningClothes.Main
{
    public partial class Panel_ListImageACTIVE : UserControl
    {
        public Image image;             //Изображение элемента
        public int ID;                  //ID элемента
        public string name;             //Имя элемента
        public int ID_Group;            //ID группы
        public string groupName;        //Группа, к которой принадлежит элемент
        public int weight;              //Вес элемента

        public bool active = false;

        public Panel_ListImageACTIVE(Panel_ListImage panel)
        {
            InitializeComponent();

            this.image = panel.image;
            this.ID = panel.ID;
            this.name = panel.name;
            this.ID_Group = panel.ID_Group;
            this.groupName = panel.groupName;
            this.weight = panel.weight;
            weightUpdate(weight);

            pictureBox.Image = image;
            labelName.Text = name;
        }

        private void Panel_ListImageName_MouseClick(object sender, MouseEventArgs e)
        {
            if (!active)
            {
                active = true;
                //Program.formMain.floodFill = true;

                BackColor = System.Drawing.Color.Lime;
                panel.BackColor = System.Drawing.SystemColors.ControlLightLight;
                pictureBox.BackColor = System.Drawing.SystemColors.ControlLightLight;

                //Показывать только выбранный слой
                Program.formMain.activeImageFloodFill(ID, image);
            }
            else if (active)
            {
                active = false;
                //Program.formMain.floodFill = false;

                BackColor = System.Drawing.SystemColors.ControlLightLight;
                panel.BackColor = System.Drawing.SystemColors.ControlLightLight;
                pictureBox.BackColor = System.Drawing.SystemColors.ControlLightLight;

                //Показывать все слои
                Program.formMain.combineImageLight();
            }
        }

        public void NoActive()
        {
            active = false;
            //Program.formMain.floodFill = false;

            BackColor = System.Drawing.SystemColors.ControlLightLight;
            panel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            pictureBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
        }


        //Значение из TrackBar в переменную и NumericUpDown
        private void weightTrackBar_Scroll(object sender, EventArgs e)
        {
                weight = weightTrackBar.Value;
                weightNumericUpDown.Value = weightTrackBar.Value;
        }
        //Значение из NumericUpDown в переменную и NumericUpDown
        private void weightNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
                weight = (int)weightNumericUpDown.Value;
                weightTrackBar.Value = (int)weightNumericUpDown.Value;
        }
        //Обновление веса
        public void weightUpdate(int weight)
        {
            this.weight = weight;
            weightTrackBar.Value = weight;
            weightNumericUpDown.Value = weight;
        }
    }
}
