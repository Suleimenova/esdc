﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemDesigningClothes.Main
{
    public partial class Panel_ImageCloth : UserControl
    {
        String pathImage;

        public Panel_ImageCloth(String pathImage)
        {
            InitializeComponent();

            this.pathImage = pathImage;
            pictureBox.Image = Image.FromFile(pathImage);
        }
    }
}
