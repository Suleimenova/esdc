﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExpertSystemDesigningClothes.Main;

namespace ExpertSystemDesigningClothes
{
    public partial class Panel_ListImage : UserControl
    {

        public Image image;             //Изображение элемента
        public int ID;                  //ID элемента
        public string name;             //Имя элемента
        public int ID_Group;            //ID группы
        public string groupName;        //Группа, к которой принадлежит элемент
        public int weight;              //Вес элемента
        public bool flagItemIsSelected; //Элемент выбран/не выбран
        public bool flagItemIsActive;   //Элемент активен/выбран    
        public bool flagWeightUpdate;   //Чтобы случайно не активировать элемент

        public Panel_ListImage(Image image, int ID, string Name, int ID_Group, string groupName)
        {
            InitializeComponent();
            
            this.image = image;
            this.ID = ID;
            this.name = Name;
            this.ID_Group = ID_Group;
            this.groupName = groupName;

            pictureBox.Image = image;
            labelName.Text = Name;
        }
        //Элемент выбран и активен (отрисовывается в главном окне)
        private void SelectedActive()
        {
            BackColor = System.Drawing.Color.Lime;
            panel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            pictureBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            flagItemIsActive = true;

            Program.formMain.addActiveElements();
        }
        //Элемент выбран и пассивен (не отрисовывается в главном окне)
        public void SelectedPassive()
        {
            BackColor = System.Drawing.Color.LightSkyBlue;
            panel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            pictureBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            flagItemIsSelected = true;
            flagItemIsActive = false;
            Program.formMain.addActiveElements();
        }
        //Элемент не выбран
        public void NoSelected()
        {
            BackColor = System.Drawing.SystemColors.ControlLightLight;
            flagItemIsActive = false;
            flagItemIsSelected = false;
            weightUpdate(0);
            Program.formMain.addActiveElements();
        }
        //Выделение элемента (активного) по клику мышки
        private void PanelForList_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (!flagItemIsActive)
                {
                    SelectedActive();
                    Program.formMain.activatedElements(ID, groupName);
                    if(weight == 0) weightUpdate(1);
                }
                else
                {
                    NoSelected();
                }
            }
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                NoSelected();          
            }
        }
        //Убрать выделение элемента (2х клик)
        private void PanelForList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            NoSelected();
        }
        //Значение из TrackBar в переменную и NumericUpDown
        private void weightTrackBar_Scroll(object sender, EventArgs e)
        {
            if (!flagWeightUpdate)
            {
                weight = weightTrackBar.Value;
                weightNumericUpDown.Value = weightTrackBar.Value;

                if (weight == 0) NoSelected();
                else
                {
                    SelectedActive();
                    Program.formMain.activatedElements(ID, groupName);
                }
            }
        }
        //Значение из NumericUpDown в переменную и NumericUpDown
        private void weightNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (!flagWeightUpdate)
            {
                weight = (int)weightNumericUpDown.Value;
                weightTrackBar.Value = (int)weightNumericUpDown.Value;
                if (weight == 0) NoSelected();
                else
                {
                    SelectedActive();
                    Program.formMain.activatedElements(ID, groupName);
                }
            }
        }
        //Обновление веса
        public void weightUpdate(int weight) 
        {
            flagWeightUpdate = true;

            this.weight = weight;
            weightTrackBar.Value = weight;
            weightNumericUpDown.Value = weight;

            flagWeightUpdate = false;

        }
    }
}
