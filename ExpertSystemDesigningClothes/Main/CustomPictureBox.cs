﻿using ExpertSystemDesigningClothes.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemDesigningClothes
{
    public partial class CustomPictureBox : PictureBox
    {
        public int ID;                  //ID элемента
        
        public CustomPictureBox(int ID)
        {
            this.ID = ID;

            //Cпособ отображения изображения
            SizeMode = PictureBoxSizeMode.Zoom;
            //Положение слоя, относительно родительского контейнера
            //Location = new Point(0, 0);
            //Прозрачность фона
            BackColor = Color.Transparent;
            //Привязка к границам формы
            Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                                                             | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));

            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseMove);
        }

        private void pictureBoxColor_MouseDown(object sender, MouseEventArgs e)
        {
            if (Program.formMain.pipette)
            {
                // Определяем цвет пикселя
                Bitmap bmp = new Bitmap(this.Width, this.Height);
                this.DrawToBitmap(bmp, new Rectangle(Point.Empty, this.Size));
                Color color = bmp.GetPixel(e.X, e.Y);
                Program.formMain.statusLabel.Text = "Цвет: R: " + color.R + " G: " + color.G + " B: " + color.B;
                Program.formMain.pictureBoxColor.BackColor = color;
            }
            else if (Program.formMain.check_ListImageActive())
            {
                Point position = new System.Drawing.Point();
                position.X = e.X * this.Image.Size.Width / this.Size.Width;
                position.Y = e.Y * this.Image.Size.Height / this.Size.Height;


                //MapFillFast map = new MapFillFast();
                //Bitmap bitmap = new System.Drawing.Bitmap(this.Image);
                //this.Image = map.Fill(this.CreateGraphics(), position, Color.Green, ref bitmap);
                ///////////////////////////////////////////////////////////////////////////////////////////////
                //FloodFillSlow map = new FloodFillSlow();
                //this.Image = FloodFillSlow.Fill(this.Image, position, Color.Green);
                ///////////////////////////////////////////////////////////////////////////////////////////////
                Bitmap bitmap = new System.Drawing.Bitmap(this.Image);
                FloodFillScanlineStack fill = new FloodFillScanlineStack(bitmap, position, Program.formMain.pictureBoxColor.BackColor);
                this.Image = fill.bmp;
                
                //Cмена изображения в группах элементов и в активных элементах
                Program.formMain.changeImage(ID, this.Image);
            }
        }

        private void pictureBoxColor_MouseMove(object sender, MouseEventArgs e)
        {
            if (Program.formMain.pipette)
            {
                // Определяем цвет пикселя
                try
                {
                    //Bitmap bmp = new Bitmap(this.Width, this.Height);
                    //this.DrawToBitmap(bmp, new Rectangle(Point.Empty, this.Size));
                    //Color color = bmp.GetPixel(e.X, e.Y);
                    //Program.formMain.statusLabel.Text = "Цвет: R: " + color.R + " G: " + color.G + " B: " + color.B;
                }
                catch 
                { 
                
                }
            }
        }
    }
}
