﻿namespace ExpertSystemDesigningClothes.Main
{
    partial class Panel_ListImageACTIVE
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.weightTrackBar = new System.Windows.Forms.TrackBar();
            this.panel = new System.Windows.Forms.Panel();
            this.labelName = new System.Windows.Forms.Label();
            this.weightNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.weightTrackBar)).BeginInit();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weightNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // weightTrackBar
            // 
            this.weightTrackBar.AutoSize = false;
            this.weightTrackBar.LargeChange = 1;
            this.weightTrackBar.Location = new System.Drawing.Point(0, 64);
            this.weightTrackBar.Name = "weightTrackBar";
            this.weightTrackBar.Size = new System.Drawing.Size(109, 28);
            this.weightTrackBar.TabIndex = 0;
            this.weightTrackBar.TabStop = false;
            this.weightTrackBar.TickFrequency = 2;
            this.weightTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.weightTrackBar.Scroll += new System.EventHandler(this.weightTrackBar_Scroll);
            // 
            // panel
            // 
            this.panel.Controls.Add(this.labelName);
            this.panel.Controls.Add(this.weightNumericUpDown);
            this.panel.Controls.Add(this.weightTrackBar);
            this.panel.Location = new System.Drawing.Point(99, 3);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(155, 95);
            this.panel.TabIndex = 4;
            this.panel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Panel_ListImageName_MouseClick);
            // 
            // labelName
            // 
            this.labelName.Location = new System.Drawing.Point(0, 3);
            this.labelName.Margin = new System.Windows.Forms.Padding(3);
            this.labelName.MaximumSize = new System.Drawing.Size(152, 58);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(152, 58);
            this.labelName.TabIndex = 2;
            this.labelName.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Panel_ListImageName_MouseClick);
            // 
            // weightNumericUpDown
            // 
            this.weightNumericUpDown.Location = new System.Drawing.Point(112, 64);
            this.weightNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.weightNumericUpDown.Name = "weightNumericUpDown";
            this.weightNumericUpDown.Size = new System.Drawing.Size(40, 23);
            this.weightNumericUpDown.TabIndex = 1;
            this.weightNumericUpDown.ValueChanged += new System.EventHandler(this.weightNumericUpDown_ValueChanged);
            // 
            // pictureBox
            // 
            this.pictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox.Location = new System.Drawing.Point(4, 3);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(95, 95);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox.TabIndex = 5;
            this.pictureBox.TabStop = false;
            this.pictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Panel_ListImageName_MouseClick);
            // 
            // Panel_ListImageACTIVE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel);
            this.Controls.Add(this.pictureBox);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MinimumSize = new System.Drawing.Size(260, 51);
            this.Name = "Panel_ListImageACTIVE";
            this.Size = new System.Drawing.Size(260, 103);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Panel_ListImageName_MouseClick);
            ((System.ComponentModel.ISupportInitialize)(this.weightTrackBar)).EndInit();
            this.panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.weightNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.TrackBar weightTrackBar;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.NumericUpDown weightNumericUpDown;
        public System.Windows.Forms.PictureBox pictureBox;


    }
}
