﻿namespace ExpertSystemDesigningClothes
{
    partial class FormMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.FileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenProjectMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.SaveImageMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveProjectMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.printImageMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.CloseMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.searchSolutionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.AboutMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.OptionsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ElementsEditor = new System.Windows.Forms.ToolStripMenuItem();
            this.ClothEditor = new System.Windows.Forms.ToolStripMenuItem();
            this.ConstraintsEditor = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.SettingsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.toolStripItem = new System.Windows.Forms.ToolStrip();
            this.comboBoxGroupElements = new System.Windows.Forms.ToolStripComboBox();
            this.selectAllButtonItem = new System.Windows.Forms.ToolStripButton();
            this.flowLayoutPanelElements = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.flowLayoutPanelColor = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanelCloth = new System.Windows.Forms.FlowLayoutPanel();
            this.panel_Color1 = new System.Windows.Forms.Panel();
            this.pictureBox_Color1 = new System.Windows.Forms.PictureBox();
            this.panel_Color2 = new System.Windows.Forms.Panel();
            this.pictureBox_Color2 = new System.Windows.Forms.PictureBox();
            this.panel_Color3 = new System.Windows.Forms.Panel();
            this.pictureBox_Color3 = new System.Windows.Forms.PictureBox();
            this.trackBarColorR = new System.Windows.Forms.TrackBar();
            this.numericUpDownColorB = new System.Windows.Forms.NumericUpDown();
            this.panelColor = new System.Windows.Forms.Panel();
            this.pictureBoxColor = new System.Windows.Forms.PictureBox();
            this.numericUpDownColorR = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.trackBarColorB = new System.Windows.Forms.TrackBar();
            this.trackBarColorG = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownColorG = new System.Windows.Forms.NumericUpDown();
            this.panel_Color4 = new System.Windows.Forms.Panel();
            this.pictureBox_Color4 = new System.Windows.Forms.PictureBox();
            this.panel_Color6 = new System.Windows.Forms.Panel();
            this.pictureBox_Color6 = new System.Windows.Forms.PictureBox();
            this.panel_Color5 = new System.Windows.Forms.Panel();
            this.pictureBox_Color5 = new System.Windows.Forms.PictureBox();
            this.saveImageDialog = new System.Windows.Forms.SaveFileDialog();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.colorScales = new ExpertSystemDesigningClothes.Main.ColorScales();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.toolStripCloth = new System.Windows.Forms.ToolStrip();
            this.comboBoxGroupCloth = new System.Windows.Forms.ToolStripComboBox();
            this.selectAllButtonCloth = new System.Windows.Forms.ToolStripButton();
            this.panel6 = new System.Windows.Forms.Panel();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.zoomNeutralButton = new System.Windows.Forms.ToolStripButton();
            this.zoomPlusButton = new System.Windows.Forms.ToolStripButton();
            this.zoomMinusButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.cursorButton = new System.Windows.Forms.ToolStripButton();
            this.pipetteButton = new System.Windows.Forms.ToolStripButton();
            this.fillButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.panel = new System.Windows.Forms.Panel();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.tableAdapterManager = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.TableAdapterManager();
            this.elementsTableAdapter = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.ElementsTableAdapter();
            this.eSDC_DataBaseDataSet = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSet();
            this.elementsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupsTableAdapter = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.GroupsTableAdapter();
            this.groupsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colorScalesTableAdapter1 = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.ColorScalesTableAdapter();
            this.printDialog = new System.Windows.Forms.PrintDialog();
            this.ColorScaleEditor = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.toolStripItem.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel_Color1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color1)).BeginInit();
            this.panel_Color2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color2)).BeginInit();
            this.panel_Color3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarColorR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColorB)).BeginInit();
            this.panelColor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColorR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarColorB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarColorG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColorG)).BeginInit();
            this.panel_Color4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color4)).BeginInit();
            this.panel_Color6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color6)).BeginInit();
            this.panel_Color5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color5)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.toolStripCloth.SuspendLayout();
            this.panel6.SuspendLayout();
            this.toolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eSDC_DataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileMenuItem,
            this.searchSolutionMenuItem,
            this.HelpMenuItem,
            this.OptionsMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip.Size = new System.Drawing.Size(904, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // FileMenuItem
            // 
            this.FileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenProjectMenuButton,
            this.toolStripSeparator2,
            this.SaveImageMenuButton,
            this.SaveProjectMenuButton,
            this.printImageMenuButton,
            this.toolStripSeparator1,
            this.CloseMenuButton});
            this.FileMenuItem.Name = "FileMenuItem";
            this.FileMenuItem.Size = new System.Drawing.Size(48, 20);
            this.FileMenuItem.Text = "Файл";
            // 
            // OpenProjectMenuButton
            // 
            this.OpenProjectMenuButton.Image = global::ExpertSystemDesigningClothes.Properties.Resources.open;
            this.OpenProjectMenuButton.Name = "OpenProjectMenuButton";
            this.OpenProjectMenuButton.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.OpenProjectMenuButton.Size = new System.Drawing.Size(279, 22);
            this.OpenProjectMenuButton.Text = "Открыть проект";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(276, 6);
            // 
            // SaveImageMenuButton
            // 
            this.SaveImageMenuButton.Image = global::ExpertSystemDesigningClothes.Properties.Resources.save2;
            this.SaveImageMenuButton.Name = "SaveImageMenuButton";
            this.SaveImageMenuButton.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.SaveImageMenuButton.Size = new System.Drawing.Size(279, 22);
            this.SaveImageMenuButton.Text = "Сохранить изображение как...";
            this.SaveImageMenuButton.Click += new System.EventHandler(this.SaveImageMenuButton_Click);
            // 
            // SaveProjectMenuButton
            // 
            this.SaveProjectMenuButton.Image = global::ExpertSystemDesigningClothes.Properties.Resources.save2;
            this.SaveProjectMenuButton.Name = "SaveProjectMenuButton";
            this.SaveProjectMenuButton.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.SaveProjectMenuButton.Size = new System.Drawing.Size(279, 22);
            this.SaveProjectMenuButton.Text = "Сохранить проект как...";
            this.SaveProjectMenuButton.Click += new System.EventHandler(this.SaveProjectMenuButton_Click);
            // 
            // printImageMenuButton
            // 
            this.printImageMenuButton.Image = global::ExpertSystemDesigningClothes.Properties.Resources.printer_16xLG;
            this.printImageMenuButton.Name = "printImageMenuButton";
            this.printImageMenuButton.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.printImageMenuButton.Size = new System.Drawing.Size(279, 22);
            this.printImageMenuButton.Text = "Печать изображения";
            this.printImageMenuButton.Click += new System.EventHandler(this.printImageMenuButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(276, 6);
            // 
            // CloseMenuButton
            // 
            this.CloseMenuButton.Image = global::ExpertSystemDesigningClothes.Properties.Resources.close3;
            this.CloseMenuButton.Name = "CloseMenuButton";
            this.CloseMenuButton.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.CloseMenuButton.Size = new System.Drawing.Size(279, 22);
            this.CloseMenuButton.Text = "Выход";
            this.CloseMenuButton.Click += new System.EventHandler(this.CloseMenuButton_Click);
            // 
            // searchSolutionMenuItem
            // 
            this.searchSolutionMenuItem.Image = global::ExpertSystemDesigningClothes.Properties.Resources.play2;
            this.searchSolutionMenuItem.Name = "searchSolutionMenuItem";
            this.searchSolutionMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.searchSolutionMenuItem.Size = new System.Drawing.Size(123, 20);
            this.searchSolutionMenuItem.Text = "Поиск решения";
            this.searchSolutionMenuItem.Click += new System.EventHandler(this.searchSolutionMenuItem_Click);
            // 
            // HelpMenuItem
            // 
            this.HelpMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.HelpMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.HelpMenuButton,
            this.AboutMenuButton});
            this.HelpMenuItem.Name = "HelpMenuItem";
            this.HelpMenuItem.Size = new System.Drawing.Size(65, 20);
            this.HelpMenuItem.Text = "Справка";
            // 
            // HelpMenuButton
            // 
            this.HelpMenuButton.Image = global::ExpertSystemDesigningClothes.Properties.Resources.help2;
            this.HelpMenuButton.Name = "HelpMenuButton";
            this.HelpMenuButton.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F1)));
            this.HelpMenuButton.Size = new System.Drawing.Size(225, 22);
            this.HelpMenuButton.Text = "Просмотр справки";
            this.HelpMenuButton.Click += new System.EventHandler(this.HelpMenuButton_Click);
            // 
            // AboutMenuButton
            // 
            this.AboutMenuButton.Image = global::ExpertSystemDesigningClothes.Properties.Resources.about2;
            this.AboutMenuButton.Name = "AboutMenuButton";
            this.AboutMenuButton.Size = new System.Drawing.Size(225, 22);
            this.AboutMenuButton.Text = "О программе";
            this.AboutMenuButton.Click += new System.EventHandler(this.AboutMenuButton_Click);
            // 
            // OptionsMenuItem
            // 
            this.OptionsMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.OptionsMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ElementsEditor,
            this.ClothEditor,
            this.ColorScaleEditor,
            this.ConstraintsEditor,
            this.toolStripSeparator3,
            this.SettingsMenuItem});
            this.OptionsMenuItem.Name = "OptionsMenuItem";
            this.OptionsMenuItem.Size = new System.Drawing.Size(56, 20);
            this.OptionsMenuItem.Text = "Опции";
            // 
            // ElementsEditor
            // 
            this.ElementsEditor.Name = "ElementsEditor";
            this.ElementsEditor.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D1)));
            this.ElementsEditor.Size = new System.Drawing.Size(250, 22);
            this.ElementsEditor.Text = "Редактор элементов";
            this.ElementsEditor.Click += new System.EventHandler(this.ElementsEditor_Click);
            // 
            // ClothEditor
            // 
            this.ClothEditor.Name = "ClothEditor";
            this.ClothEditor.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D2)));
            this.ClothEditor.Size = new System.Drawing.Size(250, 22);
            this.ClothEditor.Text = "Редактор тканей";
            this.ClothEditor.Click += new System.EventHandler(this.ClothEditor_Click);
            // 
            // ConstraintsEditor
            // 
            this.ConstraintsEditor.Name = "ConstraintsEditor";
            this.ConstraintsEditor.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D4)));
            this.ConstraintsEditor.Size = new System.Drawing.Size(250, 22);
            this.ConstraintsEditor.Text = "Редактор ограничений";
            this.ConstraintsEditor.Click += new System.EventHandler(this.ConstraintsEditor_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(247, 6);
            // 
            // SettingsMenuItem
            // 
            this.SettingsMenuItem.Image = global::ExpertSystemDesigningClothes.Properties.Resources.settings;
            this.SettingsMenuItem.Name = "SettingsMenuItem";
            this.SettingsMenuItem.Size = new System.Drawing.Size(250, 22);
            this.SettingsMenuItem.Text = "Настройки";
            this.SettingsMenuItem.Click += new System.EventHandler(this.SettingsMenuItem_Click);
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(300, 355);
            this.tabControl.TabIndex = 11;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.toolStripItem);
            this.tabPage1.Controls.Add(this.flowLayoutPanelElements);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(292, 327);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Группы элементов";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // toolStripItem
            // 
            this.toolStripItem.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.toolStripItem.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripItem.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.comboBoxGroupElements,
            this.selectAllButtonItem});
            this.toolStripItem.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.toolStripItem.Location = new System.Drawing.Point(3, 3);
            this.toolStripItem.Name = "toolStripItem";
            this.toolStripItem.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripItem.Size = new System.Drawing.Size(286, 23);
            this.toolStripItem.TabIndex = 1;
            this.toolStripItem.Text = "toolStrip2";
            // 
            // comboBoxGroupElements
            // 
            this.comboBoxGroupElements.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxGroupElements.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.comboBoxGroupElements.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.comboBoxGroupElements.Name = "comboBoxGroupElements";
            this.comboBoxGroupElements.Size = new System.Drawing.Size(260, 23);
            this.comboBoxGroupElements.SelectedIndexChanged += new System.EventHandler(this.comboBoxGroupElements_SelectedIndexChanged);
            // 
            // selectAllButtonItem
            // 
            this.selectAllButtonItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.selectAllButtonItem.AutoSize = false;
            this.selectAllButtonItem.CheckOnClick = true;
            this.selectAllButtonItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.selectAllButtonItem.Image = global::ExpertSystemDesigningClothes.Properties.Resources.selectAll;
            this.selectAllButtonItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.selectAllButtonItem.Margin = new System.Windows.Forms.Padding(2, 1, 0, 2);
            this.selectAllButtonItem.Name = "selectAllButtonItem";
            this.selectAllButtonItem.Size = new System.Drawing.Size(20, 20);
            this.selectAllButtonItem.Text = "Выделить все";
            this.selectAllButtonItem.Click += new System.EventHandler(this.selectAllButtonElements_Click);
            // 
            // flowLayoutPanelElements
            // 
            this.flowLayoutPanelElements.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.flowLayoutPanelElements.AutoScroll = true;
            this.flowLayoutPanelElements.AutoScrollMinSize = new System.Drawing.Size(0, 303);
            this.flowLayoutPanelElements.Location = new System.Drawing.Point(0, 26);
            this.flowLayoutPanelElements.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanelElements.Name = "flowLayoutPanelElements";
            this.flowLayoutPanelElements.Size = new System.Drawing.Size(285, 301);
            this.flowLayoutPanelElements.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.flowLayoutPanelColor);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(292, 327);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Активные элементы";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanelColor
            // 
            this.flowLayoutPanelColor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.flowLayoutPanelColor.AutoScroll = true;
            this.flowLayoutPanelColor.AutoScrollMinSize = new System.Drawing.Size(0, 326);
            this.flowLayoutPanelColor.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.flowLayoutPanelColor.Location = new System.Drawing.Point(0, 3);
            this.flowLayoutPanelColor.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanelColor.Name = "flowLayoutPanelColor";
            this.flowLayoutPanelColor.Size = new System.Drawing.Size(285, 324);
            this.flowLayoutPanelColor.TabIndex = 10;
            // 
            // flowLayoutPanelCloth
            // 
            this.flowLayoutPanelCloth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.flowLayoutPanelCloth.AutoScroll = true;
            this.flowLayoutPanelCloth.AutoScrollMinSize = new System.Drawing.Size(0, 147);
            this.flowLayoutPanelCloth.Location = new System.Drawing.Point(0, 26);
            this.flowLayoutPanelCloth.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanelCloth.Name = "flowLayoutPanelCloth";
            this.flowLayoutPanelCloth.Size = new System.Drawing.Size(285, 146);
            this.flowLayoutPanelCloth.TabIndex = 3;
            // 
            // panel_Color1
            // 
            this.panel_Color1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Color1.Controls.Add(this.pictureBox_Color1);
            this.panel_Color1.Location = new System.Drawing.Point(3, 38);
            this.panel_Color1.Margin = new System.Windows.Forms.Padding(6);
            this.panel_Color1.Name = "panel_Color1";
            this.panel_Color1.Size = new System.Drawing.Size(36, 36);
            this.panel_Color1.TabIndex = 15;
            // 
            // pictureBox_Color1
            // 
            this.pictureBox_Color1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Color1.Location = new System.Drawing.Point(1, 1);
            this.pictureBox_Color1.Margin = new System.Windows.Forms.Padding(1);
            this.pictureBox_Color1.Name = "pictureBox_Color1";
            this.pictureBox_Color1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox_Color1.TabIndex = 13;
            this.pictureBox_Color1.TabStop = false;
            this.pictureBox_Color1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseDown);
            this.pictureBox_Color1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseUp);
            // 
            // panel_Color2
            // 
            this.panel_Color2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Color2.Controls.Add(this.pictureBox_Color2);
            this.panel_Color2.Location = new System.Drawing.Point(52, 38);
            this.panel_Color2.Margin = new System.Windows.Forms.Padding(7);
            this.panel_Color2.Name = "panel_Color2";
            this.panel_Color2.Size = new System.Drawing.Size(36, 36);
            this.panel_Color2.TabIndex = 15;
            // 
            // pictureBox_Color2
            // 
            this.pictureBox_Color2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Color2.Location = new System.Drawing.Point(1, 1);
            this.pictureBox_Color2.Margin = new System.Windows.Forms.Padding(1);
            this.pictureBox_Color2.Name = "pictureBox_Color2";
            this.pictureBox_Color2.Size = new System.Drawing.Size(32, 32);
            this.pictureBox_Color2.TabIndex = 13;
            this.pictureBox_Color2.TabStop = false;
            this.pictureBox_Color2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseDown);
            this.pictureBox_Color2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseUp);
            // 
            // panel_Color3
            // 
            this.panel_Color3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Color3.Controls.Add(this.pictureBox_Color3);
            this.panel_Color3.Location = new System.Drawing.Point(102, 38);
            this.panel_Color3.Margin = new System.Windows.Forms.Padding(7);
            this.panel_Color3.Name = "panel_Color3";
            this.panel_Color3.Size = new System.Drawing.Size(36, 36);
            this.panel_Color3.TabIndex = 15;
            // 
            // pictureBox_Color3
            // 
            this.pictureBox_Color3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Color3.Location = new System.Drawing.Point(1, 1);
            this.pictureBox_Color3.Margin = new System.Windows.Forms.Padding(1);
            this.pictureBox_Color3.Name = "pictureBox_Color3";
            this.pictureBox_Color3.Size = new System.Drawing.Size(32, 32);
            this.pictureBox_Color3.TabIndex = 13;
            this.pictureBox_Color3.TabStop = false;
            this.pictureBox_Color3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseDown);
            this.pictureBox_Color3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseUp);
            // 
            // trackBarColorR
            // 
            this.trackBarColorR.AutoSize = false;
            this.trackBarColorR.BackColor = System.Drawing.Color.IndianRed;
            this.trackBarColorR.Location = new System.Drawing.Point(123, 97);
            this.trackBarColorR.Margin = new System.Windows.Forms.Padding(0);
            this.trackBarColorR.Maximum = 255;
            this.trackBarColorR.Name = "trackBarColorR";
            this.trackBarColorR.Size = new System.Drawing.Size(112, 19);
            this.trackBarColorR.SmallChange = 5;
            this.trackBarColorR.TabIndex = 7;
            this.trackBarColorR.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarColorR.Value = 255;
            this.trackBarColorR.Scroll += new System.EventHandler(this.trackBarColorR_Scroll);
            // 
            // numericUpDownColorB
            // 
            this.numericUpDownColorB.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericUpDownColorB.Location = new System.Drawing.Point(242, 144);
            this.numericUpDownColorB.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownColorB.Name = "numericUpDownColorB";
            this.numericUpDownColorB.Size = new System.Drawing.Size(42, 22);
            this.numericUpDownColorB.TabIndex = 5;
            this.numericUpDownColorB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownColorB.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownColorB.ValueChanged += new System.EventHandler(this.numericUpDownColorB_ValueChanged);
            // 
            // panelColor
            // 
            this.panelColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelColor.Controls.Add(this.pictureBoxColor);
            this.panelColor.Location = new System.Drawing.Point(3, 96);
            this.panelColor.MaximumSize = new System.Drawing.Size(70, 70);
            this.panelColor.MinimumSize = new System.Drawing.Size(70, 70);
            this.panelColor.Name = "panelColor";
            this.panelColor.Size = new System.Drawing.Size(70, 70);
            this.panelColor.TabIndex = 12;
            // 
            // pictureBoxColor
            // 
            this.pictureBoxColor.BackColor = System.Drawing.Color.White;
            this.pictureBoxColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxColor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxColor.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxColor.Name = "pictureBoxColor";
            this.pictureBoxColor.Size = new System.Drawing.Size(64, 64);
            this.pictureBoxColor.TabIndex = 6;
            this.pictureBoxColor.TabStop = false;
            this.pictureBoxColor.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseDown);
            this.pictureBoxColor.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseUp);
            // 
            // numericUpDownColorR
            // 
            this.numericUpDownColorR.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericUpDownColorR.Location = new System.Drawing.Point(242, 96);
            this.numericUpDownColorR.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownColorR.Name = "numericUpDownColorR";
            this.numericUpDownColorR.Size = new System.Drawing.Size(42, 22);
            this.numericUpDownColorR.TabIndex = 3;
            this.numericUpDownColorR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownColorR.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownColorR.ValueChanged += new System.EventHandler(this.numericUpDownColorR_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(79, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Red:";
            // 
            // trackBarColorB
            // 
            this.trackBarColorB.AutoSize = false;
            this.trackBarColorB.BackColor = System.Drawing.Color.LightSkyBlue;
            this.trackBarColorB.Location = new System.Drawing.Point(123, 145);
            this.trackBarColorB.Margin = new System.Windows.Forms.Padding(0);
            this.trackBarColorB.Maximum = 255;
            this.trackBarColorB.Name = "trackBarColorB";
            this.trackBarColorB.Size = new System.Drawing.Size(112, 19);
            this.trackBarColorB.SmallChange = 5;
            this.trackBarColorB.TabIndex = 9;
            this.trackBarColorB.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarColorB.Value = 255;
            this.trackBarColorB.Scroll += new System.EventHandler(this.trackBarColorB_Scroll);
            // 
            // trackBarColorG
            // 
            this.trackBarColorG.AutoSize = false;
            this.trackBarColorG.BackColor = System.Drawing.Color.LightGreen;
            this.trackBarColorG.Location = new System.Drawing.Point(123, 121);
            this.trackBarColorG.Margin = new System.Windows.Forms.Padding(0);
            this.trackBarColorG.Maximum = 255;
            this.trackBarColorG.Name = "trackBarColorG";
            this.trackBarColorG.Size = new System.Drawing.Size(112, 19);
            this.trackBarColorG.SmallChange = 5;
            this.trackBarColorG.TabIndex = 8;
            this.trackBarColorG.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarColorG.Value = 255;
            this.trackBarColorG.Scroll += new System.EventHandler(this.trackBarColorG_Scroll);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(79, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Green:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(79, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Blue:";
            // 
            // numericUpDownColorG
            // 
            this.numericUpDownColorG.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericUpDownColorG.Location = new System.Drawing.Point(242, 120);
            this.numericUpDownColorG.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownColorG.Name = "numericUpDownColorG";
            this.numericUpDownColorG.Size = new System.Drawing.Size(42, 22);
            this.numericUpDownColorG.TabIndex = 4;
            this.numericUpDownColorG.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownColorG.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownColorG.ValueChanged += new System.EventHandler(this.numericUpDownColorG_ValueChanged);
            // 
            // panel_Color4
            // 
            this.panel_Color4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Color4.Controls.Add(this.pictureBox_Color4);
            this.panel_Color4.Location = new System.Drawing.Point(151, 38);
            this.panel_Color4.Margin = new System.Windows.Forms.Padding(7);
            this.panel_Color4.Name = "panel_Color4";
            this.panel_Color4.Size = new System.Drawing.Size(36, 36);
            this.panel_Color4.TabIndex = 15;
            // 
            // pictureBox_Color4
            // 
            this.pictureBox_Color4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Color4.Location = new System.Drawing.Point(1, 1);
            this.pictureBox_Color4.Margin = new System.Windows.Forms.Padding(1);
            this.pictureBox_Color4.Name = "pictureBox_Color4";
            this.pictureBox_Color4.Size = new System.Drawing.Size(32, 32);
            this.pictureBox_Color4.TabIndex = 13;
            this.pictureBox_Color4.TabStop = false;
            this.pictureBox_Color4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseDown);
            this.pictureBox_Color4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseUp);
            // 
            // panel_Color6
            // 
            this.panel_Color6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Color6.Controls.Add(this.pictureBox_Color6);
            this.panel_Color6.Location = new System.Drawing.Point(250, 38);
            this.panel_Color6.Margin = new System.Windows.Forms.Padding(6);
            this.panel_Color6.Name = "panel_Color6";
            this.panel_Color6.Size = new System.Drawing.Size(36, 36);
            this.panel_Color6.TabIndex = 14;
            // 
            // pictureBox_Color6
            // 
            this.pictureBox_Color6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Color6.Location = new System.Drawing.Point(1, 1);
            this.pictureBox_Color6.Margin = new System.Windows.Forms.Padding(1);
            this.pictureBox_Color6.Name = "pictureBox_Color6";
            this.pictureBox_Color6.Size = new System.Drawing.Size(32, 32);
            this.pictureBox_Color6.TabIndex = 13;
            this.pictureBox_Color6.TabStop = false;
            this.pictureBox_Color6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseDown);
            this.pictureBox_Color6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseUp);
            // 
            // panel_Color5
            // 
            this.panel_Color5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Color5.Controls.Add(this.pictureBox_Color5);
            this.panel_Color5.Location = new System.Drawing.Point(201, 38);
            this.panel_Color5.Margin = new System.Windows.Forms.Padding(7);
            this.panel_Color5.Name = "panel_Color5";
            this.panel_Color5.Size = new System.Drawing.Size(36, 36);
            this.panel_Color5.TabIndex = 15;
            // 
            // pictureBox_Color5
            // 
            this.pictureBox_Color5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Color5.Location = new System.Drawing.Point(1, 1);
            this.pictureBox_Color5.Margin = new System.Windows.Forms.Padding(1);
            this.pictureBox_Color5.Name = "pictureBox_Color5";
            this.pictureBox_Color5.Size = new System.Drawing.Size(32, 32);
            this.pictureBox_Color5.TabIndex = 13;
            this.pictureBox_Color5.TabStop = false;
            this.pictureBox_Color5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseDown);
            this.pictureBox_Color5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseUp);
            // 
            // saveImageDialog
            // 
            this.saveImageDialog.Filter = "PNG|*.png|JPEG|*.jpg|GIF|*.gif|BMP|*.bmp";
            this.saveImageDialog.Title = "Сохранить изображение как...";
            // 
            // statusStrip
            // 
            this.statusStrip.BackColor = System.Drawing.SystemColors.Control;
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 579);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(904, 22);
            this.statusStrip.TabIndex = 12;
            this.statusStrip.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(0, 355);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(300, 200);
            this.tabControl1.TabIndex = 14;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.colorScales);
            this.tabPage4.Controls.Add(this.trackBarColorR);
            this.tabPage4.Controls.Add(this.panel_Color1);
            this.tabPage4.Controls.Add(this.numericUpDownColorB);
            this.tabPage4.Controls.Add(this.panel_Color2);
            this.tabPage4.Controls.Add(this.panel_Color5);
            this.tabPage4.Controls.Add(this.panelColor);
            this.tabPage4.Controls.Add(this.numericUpDownColorR);
            this.tabPage4.Controls.Add(this.panel_Color6);
            this.tabPage4.Controls.Add(this.panel_Color3);
            this.tabPage4.Controls.Add(this.label1);
            this.tabPage4.Controls.Add(this.panel_Color4);
            this.tabPage4.Controls.Add(this.numericUpDownColorG);
            this.tabPage4.Controls.Add(this.label2);
            this.tabPage4.Controls.Add(this.trackBarColorB);
            this.tabPage4.Controls.Add(this.trackBarColorG);
            this.tabPage4.Controls.Add(this.label3);
            this.tabPage4.Location = new System.Drawing.Point(4, 24);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(292, 172);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "Цвета";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // colorScales
            // 
            this.colorScales.BackColor = System.Drawing.Color.White;
            this.colorScales.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.colorScales.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.colorScales.DropDownWidth = 283;
            this.colorScales.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.colorScales.FormattingEnabled = true;
            this.colorScales.Location = new System.Drawing.Point(3, 3);
            this.colorScales.Name = "colorScales";
            this.colorScales.SelectedItem = null;
            this.colorScales.SelectedValue = new System.Drawing.Color[] {
        System.Drawing.Color.Empty,
        System.Drawing.Color.Empty,
        System.Drawing.Color.Empty,
        System.Drawing.Color.Empty,
        System.Drawing.Color.Empty,
        System.Drawing.Color.Empty};
            this.colorScales.Size = new System.Drawing.Size(283, 24);
            this.colorScales.TabIndex = 16;
            this.colorScales.SelectedIndexChanged += new System.EventHandler(this.colorScales_SelectedIndexChanged);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.toolStripCloth);
            this.tabPage5.Controls.Add(this.flowLayoutPanelCloth);
            this.tabPage5.Location = new System.Drawing.Point(4, 24);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(292, 172);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "Ткани";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // toolStripCloth
            // 
            this.toolStripCloth.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.toolStripCloth.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripCloth.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.comboBoxGroupCloth,
            this.selectAllButtonCloth});
            this.toolStripCloth.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.toolStripCloth.Location = new System.Drawing.Point(3, 3);
            this.toolStripCloth.Name = "toolStripCloth";
            this.toolStripCloth.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripCloth.Size = new System.Drawing.Size(286, 23);
            this.toolStripCloth.TabIndex = 4;
            this.toolStripCloth.Text = "toolStrip2";
            // 
            // comboBoxGroupCloth
            // 
            this.comboBoxGroupCloth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxGroupCloth.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.comboBoxGroupCloth.Items.AddRange(new object[] {
            "Все ткани"});
            this.comboBoxGroupCloth.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.comboBoxGroupCloth.Name = "comboBoxGroupCloth";
            this.comboBoxGroupCloth.Size = new System.Drawing.Size(260, 23);
            this.comboBoxGroupCloth.SelectedIndexChanged += new System.EventHandler(this.comboBoxGroupCloth_SelectedIndexChanged);
            // 
            // selectAllButtonCloth
            // 
            this.selectAllButtonCloth.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.selectAllButtonCloth.AutoSize = false;
            this.selectAllButtonCloth.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.selectAllButtonCloth.Image = global::ExpertSystemDesigningClothes.Properties.Resources.selectAll;
            this.selectAllButtonCloth.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.selectAllButtonCloth.Margin = new System.Windows.Forms.Padding(2, 1, 0, 2);
            this.selectAllButtonCloth.Name = "selectAllButtonCloth";
            this.selectAllButtonCloth.Size = new System.Drawing.Size(20, 20);
            this.selectAllButtonCloth.Text = "Выделить все";
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel6.Controls.Add(this.tabControl1);
            this.panel6.Controls.Add(this.tabControl);
            this.panel6.Location = new System.Drawing.Point(604, 24);
            this.panel6.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(300, 555);
            this.panel6.TabIndex = 15;
            // 
            // toolStrip
            // 
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripSeparator6,
            this.zoomNeutralButton,
            this.zoomPlusButton,
            this.zoomMinusButton,
            this.toolStripSeparator4,
            this.cursorButton,
            this.pipetteButton,
            this.fillButton,
            this.toolStripSeparator5,
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripSeparator7});
            this.toolStrip.Location = new System.Drawing.Point(0, 24);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip.Size = new System.Drawing.Size(24, 555);
            this.toolStrip.TabIndex = 16;
            this.toolStrip.Text = "toolStrip1";
            this.toolStrip.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::ExpertSystemDesigningClothes.Properties.Resources.bring_forward;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(21, 20);
            this.toolStripButton3.Text = "Передняя сторона";
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = global::ExpertSystemDesigningClothes.Properties.Resources.bottom;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(21, 20);
            this.toolStripButton4.Text = "Задняя сторона";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(21, 6);
            this.toolStripSeparator6.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
            // 
            // zoomNeutralButton
            // 
            this.zoomNeutralButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.zoomNeutralButton.Image = global::ExpertSystemDesigningClothes.Properties.Resources.ZoomNeutral_16xlG;
            this.zoomNeutralButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.zoomNeutralButton.Name = "zoomNeutralButton";
            this.zoomNeutralButton.Size = new System.Drawing.Size(21, 20);
            this.zoomNeutralButton.Text = "toolStripButton3";
            this.zoomNeutralButton.Click += new System.EventHandler(this.zoomNeutralButton_Click);
            // 
            // zoomPlusButton
            // 
            this.zoomPlusButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.zoomPlusButton.Image = global::ExpertSystemDesigningClothes.Properties.Resources.zoom_16xLG;
            this.zoomPlusButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.zoomPlusButton.Name = "zoomPlusButton";
            this.zoomPlusButton.Size = new System.Drawing.Size(21, 20);
            this.zoomPlusButton.Text = "Увеличить";
            this.zoomPlusButton.Click += new System.EventHandler(this.zoomPlusButton_Click);
            // 
            // zoomMinusButton
            // 
            this.zoomMinusButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.zoomMinusButton.Image = global::ExpertSystemDesigningClothes.Properties.Resources.ZoomOut_16xLG;
            this.zoomMinusButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.zoomMinusButton.Name = "zoomMinusButton";
            this.zoomMinusButton.Size = new System.Drawing.Size(21, 20);
            this.zoomMinusButton.Text = "Уменьшить";
            this.zoomMinusButton.Click += new System.EventHandler(this.zoomMinusButton_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(21, 6);
            this.toolStripSeparator4.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
            // 
            // cursorButton
            // 
            this.cursorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cursorButton.Image = global::ExpertSystemDesigningClothes.Properties.Resources.cursor;
            this.cursorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cursorButton.Name = "cursorButton";
            this.cursorButton.Size = new System.Drawing.Size(21, 20);
            this.cursorButton.Text = "toolStripButton3";
            this.cursorButton.Click += new System.EventHandler(this.cursorButton_Click);
            // 
            // pipetteButton
            // 
            this.pipetteButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pipetteButton.Enabled = false;
            this.pipetteButton.Image = global::ExpertSystemDesigningClothes.Properties.Resources.pipette;
            this.pipetteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pipetteButton.Name = "pipetteButton";
            this.pipetteButton.Size = new System.Drawing.Size(21, 20);
            this.pipetteButton.Text = "Пипетка";
            this.pipetteButton.ToolTipText = "Пипетка";
            this.pipetteButton.Click += new System.EventHandler(this.pipetteButton_Click);
            // 
            // fillButton
            // 
            this.fillButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.fillButton.Enabled = false;
            this.fillButton.Image = global::ExpertSystemDesigningClothes.Properties.Resources.fill2;
            this.fillButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fillButton.Name = "fillButton";
            this.fillButton.Size = new System.Drawing.Size(21, 20);
            this.fillButton.Text = "Заливка";
            this.fillButton.Click += new System.EventHandler(this.fillButton_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(21, 6);
            this.toolStripSeparator5.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::ExpertSystemDesigningClothes.Properties.Resources.Arrow_UndoRevertRestore_16xLG_color;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(21, 20);
            this.toolStripButton1.Text = "Отменить";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::ExpertSystemDesigningClothes.Properties.Resources.Arrow_RedoRetry_16xLG_color;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(21, 20);
            this.toolStripButton2.Text = "Вернуть";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(21, 6);
            this.toolStripSeparator7.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
            // 
            // panel
            // 
            this.panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel.AutoScroll = true;
            this.panel.AutoScrollMargin = new System.Drawing.Size(10, 10);
            this.panel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel.Location = new System.Drawing.Point(24, 24);
            this.panel.Margin = new System.Windows.Forms.Padding(0);
            this.panel.MinimumSize = new System.Drawing.Size(580, 554);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(580, 554);
            this.panel.TabIndex = 17;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "GAMS|*.gms|TXT|*.txt";
            this.saveFileDialog.Title = "Сохранить проект как...";
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ClothsTableAdapter = null;
            this.tableAdapterManager.ColorScalesTableAdapter = null;
            this.tableAdapterManager.ColorsTableAdapter = null;
            this.tableAdapterManager.ConstraintsTableAdapter = null;
            this.tableAdapterManager.ElementsTableAdapter = this.elementsTableAdapter;
            this.tableAdapterManager.GroupsTableAdapter = null;
            this.tableAdapterManager.HelpTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // elementsTableAdapter
            // 
            this.elementsTableAdapter.ClearBeforeFill = true;
            // 
            // eSDC_DataBaseDataSet
            // 
            this.eSDC_DataBaseDataSet.DataSetName = "ESDC_DataBaseDataSet";
            this.eSDC_DataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // elementsBindingSource
            // 
            this.elementsBindingSource.DataMember = "Elements";
            this.elementsBindingSource.DataSource = this.eSDC_DataBaseDataSet;
            // 
            // groupsTableAdapter
            // 
            this.groupsTableAdapter.ClearBeforeFill = true;
            // 
            // groupsBindingSource
            // 
            this.groupsBindingSource.DataMember = "Groups";
            this.groupsBindingSource.DataSource = this.eSDC_DataBaseDataSet;
            // 
            // colorScalesTableAdapter1
            // 
            this.colorScalesTableAdapter1.ClearBeforeFill = true;
            // 
            // printDialog
            // 
            this.printDialog.UseEXDialog = true;
            // 
            // ColorScaleEditor
            // 
            this.ColorScaleEditor.Name = "ColorScaleEditor";
            this.ColorScaleEditor.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D3)));
            this.ColorScaleEditor.Size = new System.Drawing.Size(250, 22);
            this.ColorScaleEditor.Text = "Редактор цветовых гамм";
            this.ColorScaleEditor.Click += new System.EventHandler(this.ColorScaleEditor_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMargin = new System.Drawing.Size(25, 25);
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(904, 601);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MinimumSize = new System.Drawing.Size(920, 640);
            this.Name = "FormMain";
            this.Text = "Экспертная система проектирования одежды";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.toolStripItem.ResumeLayout(false);
            this.toolStripItem.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.panel_Color1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color1)).EndInit();
            this.panel_Color2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color2)).EndInit();
            this.panel_Color3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarColorR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColorB)).EndInit();
            this.panelColor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColorR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarColorB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarColorG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColorG)).EndInit();
            this.panel_Color4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color4)).EndInit();
            this.panel_Color6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color6)).EndInit();
            this.panel_Color5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color5)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.toolStripCloth.ResumeLayout(false);
            this.toolStripCloth.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eSDC_DataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem OptionsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ConstraintsEditor;
        private System.Windows.Forms.ToolStripMenuItem ElementsEditor;
        private System.Windows.Forms.ToolStripMenuItem HelpMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HelpMenuButton;
        private System.Windows.Forms.ToolStripMenuItem AboutMenuButton;
        private System.Windows.Forms.ToolStripMenuItem FileMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SaveImageMenuButton;
        private System.Windows.Forms.ToolStripMenuItem CloseMenuButton;
        private System.Windows.Forms.ToolStripMenuItem SaveProjectMenuButton;
        private System.Windows.Forms.ToolStripMenuItem OpenProjectMenuButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.SaveFileDialog saveImageDialog;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelElements;
        private System.Windows.Forms.ToolStrip toolStripItem;
        private System.Windows.Forms.ToolStripComboBox comboBoxGroupElements;
        private System.Windows.Forms.ToolStripButton selectAllButtonItem;
        private System.Windows.Forms.ToolStripMenuItem ClothEditor;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelCloth;
        private System.Windows.Forms.NumericUpDown numericUpDownColorB;
        private System.Windows.Forms.NumericUpDown numericUpDownColorG;
        private System.Windows.Forms.NumericUpDown numericUpDownColorR;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar trackBarColorR;
        private System.Windows.Forms.TrackBar trackBarColorB;
        private System.Windows.Forms.TrackBar trackBarColorG;
        private System.Windows.Forms.Panel panelColor;
        private ESDC_DataBaseDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private ESDC_DataBaseDataSetTableAdapters.ElementsTableAdapter elementsTableAdapter;
        private System.Windows.Forms.ToolStripMenuItem searchSolutionMenuItem;
        private ESDC_DataBaseDataSet eSDC_DataBaseDataSet;
        private System.Windows.Forms.BindingSource elementsBindingSource;
        private ESDC_DataBaseDataSetTableAdapters.GroupsTableAdapter groupsTableAdapter;
        private System.Windows.Forms.BindingSource groupsBindingSource;
        private System.Windows.Forms.ToolStripMenuItem SettingsMenuItem;
        public System.Windows.Forms.FlowLayoutPanel flowLayoutPanelColor;
        public System.Windows.Forms.PictureBox pictureBoxColor;
        private System.Windows.Forms.Panel panel_Color1;
        private System.Windows.Forms.PictureBox pictureBox_Color1;
        private System.Windows.Forms.Panel panel_Color2;
        private System.Windows.Forms.PictureBox pictureBox_Color2;
        private System.Windows.Forms.Panel panel_Color3;
        private System.Windows.Forms.PictureBox pictureBox_Color3;
        private System.Windows.Forms.Panel panel_Color4;
        private System.Windows.Forms.PictureBox pictureBox_Color4;
        private System.Windows.Forms.Panel panel_Color6;
        private System.Windows.Forms.PictureBox pictureBox_Color6;
        private System.Windows.Forms.Panel panel_Color5;
        private System.Windows.Forms.PictureBox pictureBox_Color5;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ToolStrip toolStripCloth;
        private System.Windows.Forms.ToolStripComboBox comboBoxGroupCloth;
        private System.Windows.Forms.ToolStripButton selectAllButtonCloth;
        private ESDC_DataBaseDataSetTableAdapters.ColorScalesTableAdapter colorScalesTableAdapter1;
        private Main.ColorScales colorScales;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton zoomPlusButton;
        private System.Windows.Forms.ToolStripButton zoomMinusButton;
        private System.Windows.Forms.ToolStripButton pipetteButton;
        private System.Windows.Forms.ToolStripButton fillButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        public System.Windows.Forms.StatusStrip statusStrip;
        public System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.ToolStripButton cursorButton;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripButton zoomNeutralButton;
        public System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripMenuItem printImageMenuButton;
        private System.Windows.Forms.PrintDialog printDialog;
        private System.Windows.Forms.ToolStripMenuItem ColorScaleEditor;
    }
}

