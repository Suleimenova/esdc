﻿using ExpertSystemDesigningClothes.GAMS;
using ExpertSystemDesigningClothes.Main;
using ExpertSystemDesigningClothes.OtherForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemDesigningClothes
{
    public struct IconInfo
    {
        public bool fIcon;
        public int xHotspot;
        public int yHotspot;
        public IntPtr hbmMask;
        public IntPtr hbmColor;
    }

    public partial class FormMain : Form
    {
        public List<Panel_ListImage> panelListImage;        //Панели элементов
        List<Panel_ImageCloth> panel_ImageCloth;            //Панели тканей
        public List<CustomPictureBox> pictureBoxList;       //Слои изображения
        public Panel mainPictureBoxPanel;
        public bool floodFill = false;                      //Вкл, выкл заливку
        public bool pipette = false;                        //Вкл, выкл определение цвета
        public bool showBackground = false;                 //Вкл, выкл фоновую картинку (девушку)

        public FormMain()
        {
            InitializeComponent();

            addMainPictureBoxPanel();

            panelListImage = new List<Panel_ListImage>();
            panel_ImageCloth = new List<Panel_ImageCloth>();

            getDataSourseComboBoxGroupsElements();
            createElementsPanels();         
            
            createClothsPanels();   
         

            ///////////////////////
            colorScales.AddStandardColors();
        }

        //////////////////////////////////////////////////////////////////////////////
        //mainPictureBoxPanel
        //

        //Добавление mainPictureBoxPanel на форму (панель для обьединения слоев)
        private void addMainPictureBoxPanel()
        {
            mainPictureBoxPanel = new Panel()
            {
                //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Менять курсор только на вкладке с заливкой
                //Cursor = Cursors.Cross,
                BackColor = System.Drawing.SystemColors.ControlLightLight,
                Size = new Size(435, 500),
                //Margin = new System.Windows.Forms.Padding(25),
                Location = new System.Drawing.Point(75, 25),
                //Dock = DockStyle.Right,
                Anchor = ((System.Windows.Forms.AnchorStyles)
                    (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom))
            };
            mainPictureBoxPanel.SizeChanged += new System.EventHandler(this.mainPictureBoxPanel_SizeChanged); 

            panel.Controls.Add(mainPictureBoxPanel);
            pictureBoxList = new List<CustomPictureBox>();
            addBackground(showBackground);
        }
        
        //Добавление фонового изображения (макет девушки) на главную форму
        public void addBackground(bool showBackground)
        {
            Size size = mainPictureBoxPanel.Size;
            size.Height += 1;
            size.Width += 1;

            if (showBackground)
                pictureBoxList.Add(new CustomPictureBox(-1)
                {
                    //Задаем размер контейнера, как у родительского контейнера
                    Size = mainPictureBoxPanel.Size,
                    //Положение слоя, относительно родительского контейнера
                    Location = new Point(0, 0),
                    //Изображение слоя
                    Image = new System.Drawing.Bitmap(Properties.Resources.woman)
                });
            else
                pictureBoxList.Add(new CustomPictureBox(-1)
                {
                    //Задаем размер контейнера, как у родительского контейнера
                    Size = size,
                    //Положение слоя, относительно родительского контейнера
                    Location = new Point(0, 0)
                });


            mainPictureBoxPanel.Controls.Add(pictureBoxList[0]);
        }

        //Выбранный для заливки активный элемент
        public void activeImageFloodFill(int ID, Image image)
        {
            //Очищаем панель от предыдущих слоев
            pictureBoxList.Clear();
            mainPictureBoxPanel.Controls.Clear();
            //Добавление фонового изображения
            addBackground(showBackground);

            pictureBoxList.Add(new CustomPictureBox(ID)
            {
                //Задаем размер контейнера, как у родительского контейнера
                Size = pictureBoxList[0].Size,
                //Положение слоя, относительно родительского контейнера
                Location = new Point(0, 0),
                //Изображение слоя
                Image = image
            });

            pictureBoxList[0].Controls.Add(pictureBoxList[1]);

            //Активные элементы (убрать выделение зеленым с других элементов)
            foreach (Panel_ListImageACTIVE panel in flowLayoutPanelColor.Controls)
            {
                if (panel.ID != ID)
                    panel.NoActive();
            }
        }

        //Проверка, можно ли осуществить заливку
        public bool check_ListImageActive()
        {
            if(floodFill)
                foreach (Panel_ListImageACTIVE panel in flowLayoutPanelColor.Controls)
                {
                    if (panel.active)
                        return true;
                }
            return false;
        }

        //Меняем изображение в списке элементов после заливки
        public void changeImage(int ID, Image image) 
        {            
            foreach (Panel_ListImage panel in panelListImage)
                if (panel.ID == ID)
                {
                    panel.image = image;
                    panel.pictureBox.Update();
                }
            foreach (Panel_ListImageACTIVE panel in flowLayoutPanelColor.Controls)
                if (panel.ID == ID)
                {
                    panel.image = image;
                    panel.pictureBox.Update();
                }
        }

        //Запрет заливки на вкладке "Группы элементов" 
        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((TabControl)sender).SelectedTab.Text == "Группы элементов")
            {
                statusLabel.Text = "Открыта вкладка \"Группы элементов\". Заливка не активна.";
                floodFill = false;

                pipetteButton.Enabled = false;
                fillButton.Enabled = false;
            }
            else
                if (((TabControl)sender).SelectedTab.Text == "Активные элементы")
                {
                    statusLabel.Text = "Открыта вкладка \"Активные элементы\". Заливка cтанет активна, после выбора элемента!";
                    //floodFill = true;

                    pipetteButton.Enabled = true;
                    fillButton.Enabled = true;
                }
        }

        //Обьединение слоев изображения для показа в mainPictureBoxPanel
        public void combineImageLight()
        {
            //Очищаем панель от предыдущих слоев
            pictureBoxList.Clear();
            mainPictureBoxPanel.Controls.Clear();
            //Добавление фонового изображения
            addBackground(showBackground);

            //Добавление всех активных элементов из разных вкладок
            foreach (Panel_ListImage panel in panelListImage)
                if (panel.flagItemIsActive)
                {
                    pictureBoxList.Add(new CustomPictureBox(panel.ID)
                    {
                        //Задаем размер контейнера, как у родительского контейнера
                        Size = pictureBoxList[0].Size,
                        //Положение слоя, относительно родительского контейнера
                        Location = new Point(0, 0),
                        //Изображение слоя
                        Image = panel.image
                    });
                }

            for (int i = 1; i < pictureBoxList.Count; i++)
                pictureBoxList[i - 1].Controls.Add(pictureBoxList[i]);
        }
        
        //Обьединение слоев изображения для сохранения картинки (после этого их невозможно разделить)
        public Bitmap combineImageHard(List<Image> files)
        {
            List<Bitmap> images = new List<Bitmap>();
            Bitmap finalImage = null;
            
            try
            {
                int width = 0;
                int height = 0;

                foreach (Image image in files)
                {
                    Bitmap bitmap = new Bitmap(image);
                    //Обновляем размер bitmap
                    width = bitmap.Width;
                    height = bitmap.Height > height ? bitmap.Height : height;

                    images.Add(bitmap);
                }

                //Создаем переменную для финального изображения
                finalImage = new Bitmap(width, height);

                //С помощью графики рисуем одно изображение на другом
                using (Graphics g = Graphics.FromImage(finalImage))
                {
                    g.Clear(Color.Transparent);
                    //Обединение изображений
                    foreach (Bitmap image in images)
                    {
                        g.DrawImage(image, new Rectangle(0, 0, image.Width, image.Height));
                    }
                }
                return finalImage;
            }
            catch (Exception e)
            {
                //Console.Write(e);
                if (finalImage != null) 
                    finalImage.Dispose();
                throw;
                //return finalImage;
            }
            finally
            {
                //Очитска памяти
                foreach (Bitmap image in images)
                {
                    image.Dispose();
                }
            }
        }
        
        //При изменении размера mainPictureBoxPanel, менять размер pictureBox
        private void mainPictureBoxPanel_SizeChanged(object sender, EventArgs e)
        {
            mainPictureBoxPanel.Width = (435 * mainPictureBoxPanel.Height) / 500;

            int padding = (panel.Width - mainPictureBoxPanel.Width) / 2;
            if (padding >= 10)
                mainPictureBoxPanel.Left = padding;
            else
            {
                mainPictureBoxPanel.Left = 10;
                panel.AutoScrollMinSize = new Size(mainPictureBoxPanel.Width + 25, mainPictureBoxPanel.Height + 25);

            }

           // panel.AutoScrollMinSize.Height = 15;

           // Console.WriteLine(mainPictureBoxPanel.Left + " Size: x " + mainPictureBoxPanel.Width + " y " + mainPictureBoxPanel.Height);

            //foreach (PictureBox pictureBox in pictureBoxList)
            //    pictureBox.Size = mainPictureBoxPanel.Size;

        }


        //////////////////////////////////////////////////////////////////////////////
        //ЭЛЕМЕНТЫ ОДЕЖДЫ
        //

        //Получаем список групп элементов одежды из БД
        private void getDataSourseComboBoxGroupsElements()
        {
            ESDC_DataBaseDataSet.GroupsDataTable table = new ESDC_DataBaseDataSet.GroupsDataTable();
            groupsTableAdapter.Fill(table);
            foreach (DataRow row in table.Rows)
            {
                // Проверяем наличие в списке и если нет - добавляем
                //if (comboBoxGroupElements.FindString(row[1].ToString()) == -1)
                    comboBoxGroupElements.Items.Add(row[1].ToString());
            }
            comboBoxGroupElements.Items.Add("Все элементы");
        }

        //Создание панелей (список элементов одежды с весами)
        private void createElementsPanels()
        {
            ESDC_DataBaseDataSet.ElementsDataTable table = new ESDC_DataBaseDataSet.ElementsDataTable();
            elementsTableAdapter.Fill(table);
            int numberTypeElements = table.Count;

            foreach (DataRow row in table.Rows)
            {
                //Преобразование изображения из byte [] в Image
                MemoryStream memoryStream = new MemoryStream();
                memoryStream.Write((byte[])row["Picture"], 0, ((byte[])row["Picture"]).Length);
                Image image = Image.FromStream(memoryStream);               
                //Добавление изображения на панель
                panelListImage.Add(new Panel_ListImage(image, (int)row["ID_Element"], (string)row["Name"], (int)row["ID_Group"], get_groupName((int)row["ID_Group"])));
            }
        }

        //Получаем groupName из таблицы Groups
        private string get_groupName(int ID_Group)
        {
            string groupName = "";
            ESDC_DataBaseDataSet.GroupsDataTable table = new ESDC_DataBaseDataSet.GroupsDataTable();
            groupsTableAdapter.Fill(table);
            foreach (DataRow row in table.Rows)
            {
                if (ID_Group == (int)row["ID_Group"])
                {
                    groupName = row["Name"].ToString();
                    break;
                }
            }
            return groupName;
        }

        //Открытие определенного типа одежды
        private void comboBoxGroupElements_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Удаление изображений, перед добавлений новых
            flowLayoutPanelElements.Controls.Clear();

            foreach (Panel_ListImage panel in panelListImage)
            {
                if (panel.groupName == "" + comboBoxGroupElements.SelectedItem)
                    flowLayoutPanelElements.Controls.Add(panel);
                if (comboBoxGroupElements.SelectedItem == "Все элементы" && panel.groupName != "Cloth")
                    flowLayoutPanelElements.Controls.Add(panel);
            }
        }        
        
        //Для выделения элементов, которые были активны ранее
        public void activatedElements(int ID, string groupName)
        {
            foreach (Panel_ListImage panel in panelListImage)
                if (panel.groupName == groupName && panel.ID != ID)
                    if (panel.flagItemIsActive)
                    {
                        panel.SelectedPassive();
                    }

            combineImageLight();
        }

        //Кнопка "Выделить все" в элементах одежды
        private void selectAllButtonElements_Click(object sender, EventArgs e)
        {
            if (selectAllButtonItem.CheckState == CheckState.Checked)
            {
                statusLabel.Text = "Выделены все элементы.";
                selectAllButtonItem.ToolTipText = "Снять выделение";
                foreach (Panel_ListImage panel in panelListImage)
                    if (panel.groupName == "" + comboBoxGroupElements.SelectedItem || comboBoxGroupElements.SelectedItem == "Все элементы")
                    {                        
                        if (panel.weight == 0)
                            panel.weightUpdate(1);

                        panel.SelectedPassive();
                    }
            }
            else
            {
                statusLabel.Text = "Выделение снято.";
                selectAllButtonItem.ToolTipText = "Выделить все";
                foreach (Panel_ListImage panel in panelListImage)
                    if (panel.groupName == "" + comboBoxGroupElements.SelectedItem || comboBoxGroupElements.SelectedItem == "Все элементы")
                    {
                        panel.NoSelected();
                    }
            }
        }

        //Активные элементы (добавление)
        public void addActiveElements()
        {
            flowLayoutPanelColor.Controls.Clear();

            foreach (Panel_ListImage panel in panelListImage)
            {
                if(panel.flagItemIsActive)
                    flowLayoutPanelColor.Controls.Add(new Panel_ListImageACTIVE(panel));
            }
        }

        //////////////////////////////////////////////////////////////////////////////
        //ТКАНИ
        //

        //Создание панелей (списки тканей с весами)
        private void createClothsPanels()
        {
            try
            {
                string[] pathImage = Directory.GetFiles("Resources\\Cloth\\");
                for (int j = 0; j < pathImage.Length; j++)
                {
                    panel_ImageCloth.Add(new Panel_ImageCloth(pathImage[j]));
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Не удалось прочесть данные о тканях!\n" + e, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Открытие определенного типа ткани
        private void comboBoxGroupCloth_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Удаление изображений, перед добавлений новых
            flowLayoutPanelCloth.Controls.Clear();

            foreach (Panel_ImageCloth panel in panel_ImageCloth)
            {
                //if (panel.groupName == "Cloth")
                    flowLayoutPanelCloth.Controls.Add(panel);
                //if (comboBoxGroupCloth.SelectedItem == "Все элементы")
                //    flowLayoutPanelCloth.Controls.Add(panel);
            }
        }

        //////////////////////////////////////////////////////////////////////////////
        //ЦВЕТА
        //

        int red = 255, green = 255, blue = 255;

        private void trackBarColorR_Scroll(object sender, EventArgs e)
        {
            red = trackBarColorR.Value;
            numericUpDownColorR.Value = red;
            pictureBoxColor.BackColor = Color.FromArgb(red, green, blue);
        }
        private void trackBarColorG_Scroll(object sender, EventArgs e)
        {
            green = trackBarColorG.Value;
            numericUpDownColorG.Value = green;
            pictureBoxColor.BackColor = Color.FromArgb(red, green, blue);
        }
        private void trackBarColorB_Scroll(object sender, EventArgs e)
        {
            blue = trackBarColorB.Value;
            numericUpDownColorB.Value = blue;
            pictureBoxColor.BackColor = Color.FromArgb(red, green, blue);
        }
        private void numericUpDownColorR_ValueChanged(object sender, EventArgs e)
        {
            red = (int) numericUpDownColorR.Value;
            trackBarColorR.Value = red;
            pictureBoxColor.BackColor = Color.FromArgb(red, green, blue);
        }
        private void numericUpDownColorG_ValueChanged(object sender, EventArgs e)
        {
            green = (int)numericUpDownColorG.Value;
            trackBarColorG.Value = green;
            pictureBoxColor.BackColor = Color.FromArgb(red, green, blue);
        }
        private void numericUpDownColorB_ValueChanged(object sender, EventArgs e)
        {
            blue = (int)numericUpDownColorB.Value;
            trackBarColorB.Value = blue;
            pictureBoxColor.BackColor = Color.FromArgb(red, green, blue);
        }
        
        //Выбираем цвет (зеленая рамка и добавление цвета в список)
        private void pictureBoxColor_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right && ((Control)sender).Name != "pictureBoxColor")
                ((PictureBox)sender).BackColor = pictureBoxColor.BackColor;
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                pictureBoxColor.BackColor = ((PictureBox)sender).BackColor;


            Control formControl = (Control)sender;
            String str = "panel_Color" + formControl.Name.Substring(formControl.Name.Length - 1, 1);
            switch (str)
            {
                case "panel_Color1":
                    panel_Color1.BackColor = System.Drawing.Color.Lime;
                    break;
                case "panel_Color2":
                    panel_Color2.BackColor = System.Drawing.Color.Lime;
                    break;
                case "panel_Color3":
                    panel_Color3.BackColor = System.Drawing.Color.Lime;
                    break;
                case "panel_Color4":
                    panel_Color4.BackColor = System.Drawing.Color.Lime;
                    break;
                case "panel_Color5":
                    panel_Color5.BackColor = System.Drawing.Color.Lime;
                    break;
                case "panel_Color6":
                    panel_Color6.BackColor = System.Drawing.Color.Lime;
                    break;
                default:
                    panelColor.BackColor = System.Drawing.Color.Lime;
                    break;
            }
        }

        //Выбираем цвет (убираем зеленую рамку)
        private void pictureBoxColor_MouseUp(object sender, MouseEventArgs e)
        {
            Control formControl = (Control)sender;
            String str = "panel_Color" + formControl.Name.Substring(formControl.Name.Length - 1, 1);
            switch (str)
            {
                case "panel_Color1":
                    panel_Color1.BackColor = System.Drawing.SystemColors.ControlLightLight;
                    break;
                case "panel_Color2":
                    panel_Color2.BackColor = System.Drawing.SystemColors.ControlLightLight;
                    break;
                case "panel_Color3":
                    panel_Color3.BackColor = System.Drawing.SystemColors.ControlLightLight;
                    break;
                case "panel_Color4":
                    panel_Color4.BackColor = System.Drawing.SystemColors.ControlLightLight;
                    break;
                case "panel_Color5":
                    panel_Color5.BackColor = System.Drawing.SystemColors.ControlLightLight;
                    break;
                case "panel_Color6":
                    panel_Color6.BackColor = System.Drawing.SystemColors.ControlLightLight;
                    break;
                default:
                    panelColor.BackColor = System.Drawing.SystemColors.ControlLightLight;
                    break;
            }
        }

        //Выбираем цвет из ColorScales
        private void colorScales_SelectedIndexChanged(object sender, EventArgs e)
        {
            Color[] colors = colorScales.SelectedValue;

            pictureBox_Color1.BackColor = colors[0];
            pictureBox_Color2.BackColor = colors[1];
            pictureBox_Color3.BackColor = colors[2];
            pictureBox_Color4.BackColor = colors[3];
            pictureBox_Color5.BackColor = colors[4];
            pictureBox_Color6.BackColor = colors[5];
        }

        private void cursorButton_Click(object sender, EventArgs e)
        {
            pipette = false;
            floodFill = false;
            statusLabel.Text = "Выбран курсор.";
            toolStrip.Cursor = Cursors.Arrow;
            mainPictureBoxPanel.Cursor = Cursors.Arrow;
        }

        private void pipetteButton_Click(object sender, EventArgs e)
        {
            pipette = true;
            floodFill = false;
            statusLabel.Text = "Выбрана пипетка.";

            Cursor cursor = new Cursor(new MemoryStream(Properties.Resources.pipette2));
            // this.Cursor = cursor;
            toolStrip.Cursor = cursor;
            mainPictureBoxPanel.Cursor = cursor;
        }

        private void fillButton_Click(object sender, EventArgs e)
        {
            pipette = false;
            floodFill = true;
            statusLabel.Text = "Выбрана заливка.";

            //Cursor cursor = new Cursor(new MemoryStream(Properties.Resources.fill1));
            // this.Cursor = cursor;
            //mainPictureBoxPanel.Cursor = cursor;


            Bitmap bitmap = new Bitmap(140, 25);
            Graphics g = Graphics.FromImage(bitmap);
            //using (Font f = new Font(FontFamily.GenericSansSerif, 10))
            //  g.DrawImage(Properties.Resources.fill,);

            Cursor cursor = CreateCursor((Bitmap)Properties.Resources.fill2, 0, 14);
            toolStrip.Cursor = cursor;
            mainPictureBoxPanel.Cursor = cursor;

            bitmap.Dispose();
        }

        //////////////////////////////////////////////////////////////////////////////
        //CREATE CURSOR
        //

        [DllImport("user32.dll")]
        public static extern IntPtr CreateIconIndirect(ref IconInfo icon);
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetIconInfo(IntPtr hIcon, ref IconInfo pIconInfo);

        public static Cursor CreateCursor(Bitmap bmp, int xHotSpot, int yHotSpot)
        {
            IntPtr ptr = bmp.GetHicon();
            IconInfo tmp = new IconInfo();
            GetIconInfo(ptr, ref tmp);
            tmp.xHotspot = xHotSpot;
            tmp.yHotspot = yHotSpot;
            tmp.fIcon = false;
            ptr = CreateIconIndirect(ref tmp);
            return new Cursor(ptr);
        }
        
        //////////////////////////////////////////////////////////////////////////////
        //Далее, обработка нажатий на различные кнопки
        //
        
        //Сохранение результата (изображение)
        private void SaveImageMenuButton_Click(object sender, EventArgs e)
        {
            List<Image> files = new List<Image>();
            foreach (Panel_ListImage panel in panelListImage)
                if(panel.flagItemIsActive)
                    files.Add(panel.image);

            if (files.Count != 0)
            {
                Bitmap bitmap = combineImageHard(files);
                if (saveImageDialog.ShowDialog() == DialogResult.OK)
                    bitmap.Save(saveImageDialog.FileName);
            }
            else 
            {
                MessageBox.Show("Изображение пустое!\n", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //Сохранение результата (проект)
        private void SaveProjectMenuButton_Click(object sender, EventArgs e)
        {
            FormGAMS gams = new FormGAMS(panelListImage);

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filename = saveFileDialog.FileName;

                System.IO.FileInfo file = new System.IO.FileInfo(filename);
                System.IO.StreamWriter write_text;
                write_text = file.CreateText();
                write_text.WriteLine(gams.saveProject());   //Записываем в файл текст
                write_text.Close();
            }
        }
        //Печать изображения
        private void printImageMenuButton_Click(object sender, EventArgs e)
        {
            //printDialog.ShowDialog();
            //pageSetupDialog1.ShowDialog();

            System.Drawing.Printing.PrintDocument Document = new System.Drawing.Printing.PrintDocument();
            Document.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(Document_PrintPage);
                DialogResult result = printDialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    Document.Print();
                }
        }
        private void Document_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            List<Image> files = new List<Image>();
            foreach (Panel_ListImage panel in panelListImage)
                if (panel.flagItemIsActive)
                    files.Add(panel.image);

            if (files.Count != 0)
            {
                Bitmap bitmap = combineImageHard(files);
                e.Graphics.DrawImage(bitmap, new Point(0, 0)); //Картинка на печать
            }
            else 
            {
                MessageBox.Show("Изображение пустое!\n", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //Редактор ограничений (добавление)
        private void ConstraintsEditor_Click(object sender, EventArgs e)
        {
            FormEditor editor = new FormEditor(panelListImage);
            editor.typePanel("ListConstraints");
            editor.ShowDialog();
        }
        //Редактор элементов (список тканей)
        private void ClothEditor_Click(object sender, EventArgs e)
        {
            FormEditor editor = new FormEditor(panelListImage);
            editor.typePanel("ListCloths");
            editor.ShowDialog();

        }
        //Редактор элементов (список элементов одежды)
        private void ElementsEditor_Click(object sender, EventArgs e)
        {
            FormEditor editor = new FormEditor(panelListImage);
            editor.typePanel("ListElements");
            editor.ShowDialog();
        }
        //Редактор цветовых гамм
        private void ColorScaleEditor_Click(object sender, EventArgs e)
        {
            FormEditor editor = new FormEditor(panelListImage);
            editor.typePanel("EditColorScales");
            editor.ShowDialog();
        }
        //Вызов окна "Справка"
        private void HelpMenuButton_Click(object sender, EventArgs e)
        {
            Help_ESDC help = new Help_ESDC();
            help.Show();
        }       
        //Вызов окна "О программе"
        private void AboutMenuButton_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.ShowDialog();
        }
        //Закрыть программу
        private void CloseMenuButton_Click(object sender, EventArgs e)
        {
            Close();
        }
        //Загрузка данных
        private void FormMain_Load(object sender, EventArgs e)
        {
            this.elementsTableAdapter.Fill(this.eSDC_DataBaseDataSet.Elements);
        }
        //Поиск решения
        private void searchSolutionMenuItem_Click(object sender, EventArgs e)
        {
            FormGAMS gams = new FormGAMS(panelListImage);
            gams.ShowDialog();
        }
        //Настройки
        private void SettingsMenuItem_Click(object sender, EventArgs e)
        {
            //Settings settings = new Settings();
            //settings.ShowDialog();

            FormEditor editor = new FormEditor(panelListImage);
            editor.typePanel("Settings");
            editor.ShowDialog();
        }

        //Кнопки зума изображения
        int zoom = 2;
        Size defaultSize = new Size(920, 640);

        private void zoomPlusButton_Click(object sender, EventArgs e)
        {
            if (zoom < 6)
            {
                mainPictureBoxPanel.Height += 100;
                zoom++;
            }
        }

        private void zoomMinusButton_Click(object sender, EventArgs e)
        {
            if (zoom > 0)
            {
                mainPictureBoxPanel.Height -= 100;
                zoom--;
            }           
        }

        private void zoomNeutralButton_Click(object sender, EventArgs e)
        {
            if (Program.formMain.Size == defaultSize)
            {
                zoom = 2;
                mainPictureBoxPanel.Size = new Size(435, 500);
            }
        }
    }
}
