﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ExpertSystemDesigningClothes
{
    /// <summary>
    /// Панель, для добавления элементов в БД 
    /// </summary>
    public partial class Panel_AddElement : UserControl
    {
        FormEditor editor;

        Boolean flagEdit;

        public Panel_AddElement(FormEditor editor)
        {
            InitializeComponent();

            this.editor = editor;
            getDataSourseComboBoxGroups();
        }

        //Редактирование записи?
        public void editRow(Boolean flagEdit) 
        {
            this.flagEdit = flagEdit;
            if (flagEdit)
            {
                int editID_Element = editor.editID_Element;

                ESDC_DataBaseDataSet.ElementsDataTable table = new ESDC_DataBaseDataSet.ElementsDataTable();
                elementsTableAdapter.Fill(table);

                foreach (DataRow row in table.Rows)
                {
                    // Проверяем наличие в списке и если нет - добавляем
                    if (editID_Element == (int)row[0])
                    {
                        textBoxName.Text = row["Name"].ToString();
                        comboBoxGroups.Text = getName_Group((int)row["ID_Group"]);
                        numericUpDownX.Value = (int)row["Location_X"];
                        numericUpDownY.Value = (int)row["Location_Y"];

                        //Преобразование изображения из byte [] в Image
                        MemoryStream memoryStream = new MemoryStream();
                        //Загрузка изображения (перед) в pictureBox
                        memoryStream.Write((byte[])row["Picture"], 0, ((byte[])row["Picture"]).Length);
                        pictureBox.Image = Image.FromStream(memoryStream);
                        //Загрузка изображения (зад) в pictureBoxBack
                        //memoryStream.Write((byte[])row["Picture_Back"], 0, ((byte[])row["Picture_Back"]).Length);
                        //pictureBoxBack.Image = Image.FromStream(memoryStream);

                        break;
                    }
                    else Console.WriteLine("int = " + editID_Element + " != " + (int)row[0]);
                }
            }        
        }
        //Сохранение данных в БД
        private void elementsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (pictureBox.Image != null)
            {
                try
                {
                    if (getID_Group(comboBoxGroups.SelectedItem.ToString()) != -1)
                    {
                        ESDC_DataBaseDataSet.ElementsRow newRow = eSDC_DataBaseDataSet.Elements.NewElementsRow();

                        if (flagEdit) newRow.ID_Element = editor.editID_Element;

                        newRow.Name = textBoxName.Text;
                        newRow.Location_X = (int)numericUpDownX.Value;
                        newRow.Location_Y = (int)numericUpDownY.Value;

                        newRow.ID_Group = getID_Group(comboBoxGroups.SelectedItem.ToString());

                        //Загрузка изображения (перед) в виде массива byte[]
                        MemoryStream memoryStream = new System.IO.MemoryStream();
                        pictureBox.Image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
                        byte[] b = memoryStream.ToArray();
                        newRow.Picture = b;
                        //Загрузка изображения (зад) в виде массива byte[]
                        //pictureBoxBack.Image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
                        //b = memoryStream.ToArray();
                        //newRow.Picture_Back = b;

                        //Если не редактирование, то добавляем новую строку, иначе ищем строку по ID и меняем в неё поля
                        if (!flagEdit)
                            eSDC_DataBaseDataSet.Elements.Rows.Add(newRow);
                        else
                        {
                            ESDC_DataBaseDataSet.ElementsRow editRow = eSDC_DataBaseDataSet.Elements.FindByID_Element(newRow.ID_Element);

                            editRow.Name = newRow.Name;
                            editRow.Location_X = newRow.Location_X;
                            editRow.Location_Y = newRow.Location_Y;
                            editRow.ID_Group = newRow.ID_Group;
                            editRow.Picture = newRow.Picture;
                            //editRow.Picture_Back = newRow.Picture_Back;
                        }


                        this.Validate();
                        this.elementsBindingSource.EndEdit();
                        this.tableAdapterManager.UpdateAll(this.eSDC_DataBaseDataSet);
                    }
                }
                catch
                {
                    MessageBox.Show("Невозможно прочитать название группы!" + comboBoxGroups.SelectedItem.ToString(), "Ошибка чтения данных!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
                MessageBox.Show("Загрузите изображение перед добавлением элемента!", "Отсутствует изображение!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        //Получаем список групп элементов из БД
        private void getDataSourseComboBoxGroups()
        {
            ESDC_DataBaseDataSet.GroupsDataTable table = new ESDC_DataBaseDataSet.GroupsDataTable();
            groupsTableAdapter.Fill(table);
            foreach (DataRow row in table.Rows)
            {
                // Проверяем наличие в списке и если нет - добавляем
                if (comboBoxGroups.FindString(row[1].ToString()) == -1)
                    comboBoxGroups.Items.Add(row[1].ToString());
            }
        }
        //Получаем ID_Group для сохранения в таблицу Elements
        private int getID_Group(String name) 
        {
            int ID_Group = -1;
            ESDC_DataBaseDataSet.GroupsDataTable table = new ESDC_DataBaseDataSet.GroupsDataTable();
            groupsTableAdapter.Fill(table);
            foreach (DataRow row in table.Rows)
                if (name == row[1].ToString())
                {
                    ID_Group = (int)row[0];
                    break;
                }
            return ID_Group;
        }
        //
        private String getName_Group(int ID_Group)
        {
            String name = "Ошибка при чтении имени группы!";
            ESDC_DataBaseDataSet.GroupsDataTable table = new ESDC_DataBaseDataSet.GroupsDataTable();
            groupsTableAdapter.Fill(table);
            foreach (DataRow row in table.Rows)
                if (ID_Group == (int) row[0])
                {
                    name = row["Name"].ToString();
                    break;
                }
            return name;
        }
        //Загрузка изображения (перед)
        private void pictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                pictureBox.Image = Image.FromFile(openFileDialog.FileName);
            }
        }
        //Загрузка изображения (зад)
        private void pictureBoxBack_MouseClick(object sender, MouseEventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                pictureBoxBack.Image = Image.FromFile(openFileDialog.FileName);
            }
        }
        //Кнопка "Отмена"
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            flagEdit = false;
            pictureBox.Image = null;
            pictureBoxBack.Image = null;
            textBoxName.Text = null;
            numericUpDownX.Value = 0;
            numericUpDownY.Value = 0;
            comboBoxGroups.SelectedIndex = -1;
            
            //editor.Close();
        }
        //Загрузка данных
        private void PanelEditor_AddElements_Load(object sender, EventArgs e)
        {
            this.elementsTableAdapter.Fill(this.eSDC_DataBaseDataSet.Elements);
            this.groupsTableAdapter.Fill(this.eSDC_DataBaseDataSet.Groups);
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            if ((sender as CheckBox).Checked)
                pictureBoxBack.BackgroundImage = Properties.Resources.woman;//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            else
                pictureBoxBack.BackgroundImage = null;
        }
    }
}
