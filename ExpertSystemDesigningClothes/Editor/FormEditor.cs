﻿using ExpertSystemDesigningClothes.Editor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemDesigningClothes
{
    public partial class FormEditor : Form
    {
        Panel_ListElements listElements;
        Panel_ListCloths listCloths;
        Panel_ListConstraints listConstraints;

        Panel_AddCloth addCloth;
        Panel_AddConstraint addConst;
        Panel_AddElement addElement;

        Panel_EditorColorScales editorCS;

        Panel_Settings settings;

        public int editID_Element;
        public int editID_Constraint;

        public FormEditor(List<Panel_ListImage> panelListImage)
        {
            InitializeComponent();

            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            listElements = new Panel_ListElements(this);
            listCloths   = new Panel_ListCloths(this);
            listConstraints = new Panel_ListConstraints(this);

            addCloth    = new Panel_AddCloth();
            addConst = new Panel_AddConstraint(this, panelListImage);
            addElement  = new Panel_AddElement(this);

            editorCS = new Panel_EditorColorScales();

            settings = new Panel_Settings();

            treeView.ExpandAll();
        }

        public void typePanel (String type)
        {
            if (type == "AddElement")
            {
                treeView.SelectedNode = treeView.Nodes.Find("AddElement", true)[0];
                panel.Controls.Clear();
                
                addElement.Size = panel.Size;
                panel.Controls.Add(addElement);
            }
            if (type == "EditElement")
            {
                treeView.SelectedNode = treeView.Nodes.Find("AddElement", true)[0];
                panel.Controls.Clear();
                
                addElement.Size = panel.Size;
                addElement.editRow(true);
                panel.Controls.Add(addElement);
            }
            if (type == "ListElements")
            {
                treeView.SelectedNode = treeView.Nodes.Find("ListElements", true)[0];
                panel.Controls.Clear();
                
                listElements.Size = panel.Size;
                listElements.DataUpdate();
                panel.Controls.Add(listElements);
            }
            if (type == "AddCloth")
            {
                treeView.SelectedNode = treeView.Nodes.Find("AddCloth", true)[0];
                panel.Controls.Clear();
                addCloth.Size = panel.Size;
                panel.Controls.Add(addCloth);
            }
            if (type == "ListCloths")
            {
                treeView.SelectedNode = treeView.Nodes.Find("ListCloths", true)[0];
                panel.Controls.Clear();

                listCloths.Size = panel.Size;
                listCloths.DataUpdate();
                panel.Controls.Add(listCloths);
            }
            if (type == "AddConstraint")
            {
                treeView.SelectedNode = treeView.Nodes.Find("AddConstraint", true)[0];
                panel.Controls.Clear();
                addConst.Size = panel.Size;
                panel.Controls.Add(addConst);
            }
            if (type == "EditConstraint")
            {
                treeView.SelectedNode = treeView.Nodes.Find("AddConstraint", true)[0];
                panel.Controls.Clear();

                addConst.Size = panel.Size;
                addConst.editRow(true);
                panel.Controls.Add(addConst);
            }
            if (type == "ListConstraints")
            {
                treeView.SelectedNode = treeView.Nodes.Find("ListConstraints", true)[0];
                panel.Controls.Clear();

                listConstraints.Size = panel.Size;
                listConstraints.DataUpdate();
                panel.Controls.Add(listConstraints);
            }
            if (type == "Settings")
            {
                treeView.SelectedNode = treeView.Nodes.Find("rootSettings", true)[0];
                panel.Controls.Clear();

                settings.Size = panel.Size;
                panel.Controls.Add(settings);
            }
            if (type == "EditColorScales")
            {
                panel.Controls.Clear();

                editorCS.Size = panel.Size;
                panel.Controls.Add(editorCS);
            }
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Name == "AddElement" || e.Node.Name == "rootOne")
            {
                panel.Controls.Clear();
                addElement.Size = panel.Size;
                panel.Controls.Add(addElement);
            }
            if (e.Node.Name == "ListElements")
            {
                panel.Controls.Clear();

                listElements.Size = panel.Size;
                listElements.DataUpdate();
                panel.Controls.Add(listElements);
            }
            if (e.Node.Name == "AddCloth" || e.Node.Name == "rootTwo")
            {
                panel.Controls.Clear();

                addCloth.Size = panel.Size;
                panel.Controls.Add(addCloth);
            }
            if (e.Node.Name == "ListCloths")
            {
                panel.Controls.Clear();

                listCloths.Size = panel.Size;
                listCloths.DataUpdate();
                panel.Controls.Add(listCloths);
            }
            if (e.Node.Name == "AddConstraint" || e.Node.Name == "rootThree")
            {
                panel.Controls.Clear();

                addConst.Size = panel.Size;
                panel.Controls.Add(addConst);
            }
            if (e.Node.Name == "ListConstraints")
            {
                panel.Controls.Clear();

                listConstraints.Size = panel.Size;
                listConstraints.DataUpdate();
                panel.Controls.Add(listConstraints);
            }
            if (e.Node.Name == "rootSettings")
            {
                panel.Controls.Clear();

                settings.Size = panel.Size;
                panel.Controls.Add(settings);
            }
            if (e.Node.Name == "EditColorScales" || e.Node.Name == "rootFour")
            {
                panel.Controls.Clear();

                editorCS.Size = panel.Size;
                panel.Controls.Add(editorCS);
            }
        }

        //При изменении размера EditorForm, менять размер PanelEditor_List
        private void EditorForm_SizeChanged(object sender, EventArgs e)
        {
            listElements.Size    = panel.Size;
            listCloths.Size      = panel.Size;
            listConstraints.Size = panel.Size;
            addConst.Size        = panel.Size;
            addElement.Size      = panel.Size;
            editorCS.Size        = panel.Size;
        }

        //Передача строки для редактирования элемента
        public void setID_Element(int editID_Element) 
        {
            this.editID_Element = editID_Element;
        }

        //Передача строки для редактирования ограничения
        public void setID_Constraint(int editID_Constraint)
        {
            this.editID_Constraint = editID_Constraint;
        }
    }
}
