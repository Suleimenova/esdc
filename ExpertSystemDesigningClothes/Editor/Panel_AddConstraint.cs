﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ExpertSystemDesigningClothes
{
    /// <summary>
    /// Панель, для добавления ограничений в БД 
    /// </summary>
    
    public partial class Panel_AddConstraint : UserControl
    {
        FormEditor editor;
        List<Panel_ListImage> panelListImage;
        Boolean flagEdit;

        int rows = 0,       //rows: количество групп, 
            columns = 0;    //columns: количество элементов в группе
        int[,] matrix_ID;      //Матрица элементов, для показа изображений элементов


        public Panel_AddConstraint(FormEditor editor, List<Panel_ListImage> panelListImage)
        {
            InitializeComponent();

            this.editor = editor;
            this.panelListImage = panelListImage;

            CreateMatrix();
        }

        private void CreateMatrix()
        {
            // 1 Поперечное членение стана               
            // 2 Продольное членение стана               
            // 3 Застежки                                
            // 4 Рукава                                  
            // 5 Карманы                                 
            // 6 Пояса                                   
            // 7 Воротник                                
            // 8 Дополнительные составляющие  
            //=l= меньше чем или равно;              
            //=g= больше чем или равно;              
            //=e= равно. 

            ESDC_DataBaseDataSet.GroupsDataTable table = new ESDC_DataBaseDataSet.GroupsDataTable();
            groupsTableAdapter.Fill(table);
            rows = table.Rows.Count;


            //Алгоритм ужасен
            //Записываем все веса элементов в datagridview
            //Так как элементы хранятся в списке, то приходится делать сортировку.          

            int[] massrows = new int[rows];

            foreach (Panel_ListImage panel in panelListImage)
            {//Ищем количество элементов в каждой группе
                massrows[panel.ID_Group - 1] += 1;
                if (massrows[panel.ID_Group - 1] > columns)
                    columns = massrows[panel.ID_Group - 1];
            }

            dataGridView.ColumnCount = columns;
            dataGridView.RowCount = rows;

            matrix_ID = new int[columns, rows];

            //Название групп элементов
            int k = 0;
            foreach (DataRow row in table.Rows)
            {
                if (row["Type"].ToString() == "Elements  ")
                {
                    if (row["Name"].ToString() == "Поперечное членение стана")
                        dataGridView.Rows[k].HeaderCell.Value = "Поп.член.стана";
                    else if (row["Name"].ToString() == "Продольное членение стана")
                        dataGridView.Rows[k].HeaderCell.Value = "Прод.член.стана";
                    else if (row["Name"].ToString() == "Дополнительные составляющие")
                        dataGridView.Rows[k].HeaderCell.Value = "Доп.составляющие";
                    else
                        dataGridView.Rows[k].HeaderCell.Value = row["Name"].ToString();
                    k++;
                }
                if (k > rows - 1)
                    break;
            }


            //Костыль
            for (int i = 0; i < rows; i++)
                for (int j = 0; j < columns; j++)
                {
                    dataGridView[j, i].Style.BackColor = Color.LightGray;
                    //dataGridView[j, i].Value = 0;
                }

            foreach (Panel_ListImage panel in panelListImage)
            {
                for (int i = 0; i < rows; i++)
                    if (panel.ID_Group == i + 1)
                        for (int j = 0; j < columns; j++)
                            if (dataGridView[j, i].Value == null)
                            {
                                matrix_ID[j, i] = panel.ID;

                                //dataGridView[j, i].Value = panel.weight;
                                //if (panel.weight != 0)
                                //{
                                    dataGridView[j, i].Value = j+1;
                                    dataGridView[j, i].Style.BackColor = Color.LightSkyBlue;
                                //}
                                //else
                                //   dataGridView[j, i].Style.BackColor = Color.LightGray;
                                break;
                            }
            }
        }

        //Сохраняем ограничение в БД
        private void buttonADD_Click(object sender, EventArgs e)
        {
            if (textBoxConstraint.Text != "")
            {
                    ESDC_DataBaseDataSet.ConstraintsRow newRow = eSDC_DataBaseDataSet.Constraints.NewConstraintsRow();

                    if (flagEdit) 
                        newRow.ID_Constraint = editor.editID_Constraint;

                    newRow.Constraint = textBoxConstraint.Text.ToString();
                    newRow.Description = textBoxDescription.Text.ToString();

                    //Если не редактирование, то добавляем новую строку, иначе ищем строку по ID и меняем в неё поля
                    if (!flagEdit)
                        eSDC_DataBaseDataSet.Constraints.Rows.Add(newRow);
                    else
                    {
                        ESDC_DataBaseDataSet.ConstraintsRow editRow = eSDC_DataBaseDataSet.Constraints.FindByID_Constraint(newRow.ID_Constraint);

                        editRow.Constraint = textBoxConstraint.Text.ToString();
                        editRow.Description = textBoxDescription.Text.ToString();
                    }


                    this.Validate();
                    this.constraintsBindingSource.EndEdit();
                    this.tableAdapterManager.UpdateAll(this.eSDC_DataBaseDataSet);
            }
            else
                MessageBox.Show("Напишите ограничение перед сохранением!", "Отсутствует текст!", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        //Редактирование записи
        public void editRow(Boolean flagEdit)
        {
            this.flagEdit = flagEdit;
            
            if (flagEdit)
            {
                int editID_Constraint = editor.editID_Constraint;

                ESDC_DataBaseDataSet.ConstraintsDataTable table = new ESDC_DataBaseDataSet.ConstraintsDataTable();
                constraintsTableAdapter.Fill(table);

                foreach (DataRow row in table.Rows)
                {
                    // Проверяем наличие в списке и если нет - добавляем
                    if (editID_Constraint == (int)row[0])
                    {
                        textBoxConstraint.Text = row["Constraint"].ToString();
                        textBoxDescription.Text = row["Description"].ToString();
                        
                        break;
                    }
                    else Console.WriteLine("int = " + editID_Constraint + " != " + (int)row[0]);
                }


            }
        }

        //Показ изображения элемента
        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (Panel_ListImage panel in panelListImage)
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                    if (matrix_ID[e.ColumnIndex, e.RowIndex] == panel.ID)
                        pictureBoxElement.Image = panel.image;
            }
        }

        //Выбор элемента
        private void dataGridView_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left && ((Control)sender).Name != "pictureBoxColor")
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                    textBoxConstraint.Text += "+y(\'" + (e.RowIndex + 1) + "\',\'" + (e.ColumnIndex + 1) + "\')";
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                    textBoxConstraint.Text += "-y(\'" + (e.RowIndex + 1) + "\',\'" + (e.ColumnIndex + 1) + "\')";
        }

        private void buttonLogicalOperations_Click(object sender, EventArgs e)
        {
            if ((sender as ToolStripButton).Name == "buttonEQUALITY")
                textBoxConstraint.Text += "=e=";
            if ((sender as ToolStripButton).Name == "buttonLESSOREQUAL")
                textBoxConstraint.Text += "=l=";
            if ((sender as ToolStripButton).Name == "buttonGREATEROREQUAL")
                textBoxConstraint.Text += "=g=";
        }

        //Загрузка данных
        private void Panel_AddConstraint_Load(object sender, EventArgs e)
        {
            //this.elementsTableAdapter.Fill(this.eSDC_DataBaseDataSet.Elements);
            this.constraintsTableAdapter.Fill(this.eSDC_DataBaseDataSet.Constraints);
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            textBoxConstraint.Text = null;
            textBoxDescription.Text = null;
        }
    }
}
