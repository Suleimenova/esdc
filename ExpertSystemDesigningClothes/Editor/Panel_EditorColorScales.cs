﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemDesigningClothes.Editor
{
    public partial class Panel_EditorColorScales : UserControl
    {

        int red = 255, green = 255, blue = 255;
        int ID_ColorScale = -1;
        List<int> ID_Colors;

        public Panel_EditorColorScales()
        {
            InitializeComponent();
            colorScalesList.AddDBColors(colorsTableAdapter, colorScalesTableAdapter);
        }

        private void trackBarColorR_Scroll(object sender, EventArgs e)
        {
            red = trackBarColorR.Value;
            numericUpDownColorR.Value = red;
            pictureBoxColor.BackColor = Color.FromArgb(red, green, blue);
        }
        private void trackBarColorG_Scroll(object sender, EventArgs e)
        {
            green = trackBarColorG.Value;
            numericUpDownColorG.Value = green;
            pictureBoxColor.BackColor = Color.FromArgb(red, green, blue);
        }
        private void trackBarColorB_Scroll(object sender, EventArgs e)
        {
            blue = trackBarColorB.Value;
            numericUpDownColorB.Value = blue;
            pictureBoxColor.BackColor = Color.FromArgb(red, green, blue);
        }
        private void numericUpDownColorR_ValueChanged(object sender, EventArgs e)
        {
            red = (int)numericUpDownColorR.Value;
            trackBarColorR.Value = red;
            pictureBoxColor.BackColor = Color.FromArgb(red, green, blue);
        }
        private void numericUpDownColorG_ValueChanged(object sender, EventArgs e)
        {
            green = (int)numericUpDownColorG.Value;
            trackBarColorG.Value = green;
            pictureBoxColor.BackColor = Color.FromArgb(red, green, blue);
        }
        private void numericUpDownColorB_ValueChanged(object sender, EventArgs e)
        {
            blue = (int)numericUpDownColorB.Value;
            trackBarColorB.Value = blue;
            pictureBoxColor.BackColor = Color.FromArgb(red, green, blue);
        }

        //Выбираем цвет (зеленая рамка и добавление цвета в список)
        private void pictureBoxColor_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right && ((Control)sender).Name != "pictureBoxColor")
                ((PictureBox)sender).BackColor = pictureBoxColor.BackColor;
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                pictureBoxColor.BackColor = ((PictureBox)sender).BackColor;


            Control formControl = (Control)sender;
            String str = "panel_Color" + formControl.Name.Substring(formControl.Name.Length - 1, 1);
            switch (str)
            {
                case "panel_Color1":
                    panel_Color1.BackColor = System.Drawing.Color.Lime;
                    break;
                case "panel_Color2":
                    panel_Color2.BackColor = System.Drawing.Color.Lime;
                    break;
                case "panel_Color3":
                    panel_Color3.BackColor = System.Drawing.Color.Lime;
                    break;
                case "panel_Color4":
                    panel_Color4.BackColor = System.Drawing.Color.Lime;
                    break;
                case "panel_Color5":
                    panel_Color5.BackColor = System.Drawing.Color.Lime;
                    break;
                case "panel_Color6":
                    panel_Color6.BackColor = System.Drawing.Color.Lime;
                    break;
                default:
                    panelColor.BackColor = System.Drawing.Color.Lime;
                    break;
            }
        }

        //Выбираем цвет (убираем зеленую рамку)
        private void pictureBoxColor_MouseUp(object sender, MouseEventArgs e)
        {
            Control formControl = (Control)sender;
            String str = "panel_Color" + formControl.Name.Substring(formControl.Name.Length - 1, 1);
            switch (str)
            {
                case "panel_Color1":
                    panel_Color1.BackColor = System.Drawing.SystemColors.ControlLightLight;
                    break;
                case "panel_Color2":
                    panel_Color2.BackColor = System.Drawing.SystemColors.ControlLightLight;
                    break;
                case "panel_Color3":
                    panel_Color3.BackColor = System.Drawing.SystemColors.ControlLightLight;
                    break;
                case "panel_Color4":
                    panel_Color4.BackColor = System.Drawing.SystemColors.ControlLightLight;
                    break;
                case "panel_Color5":
                    panel_Color5.BackColor = System.Drawing.SystemColors.ControlLightLight;
                    break;
                case "panel_Color6":
                    panel_Color6.BackColor = System.Drawing.SystemColors.ControlLightLight;
                    break;
                default:
                    panelColor.BackColor = System.Drawing.SystemColors.ControlLightLight;
                    break;
            }
        }

        //Выбираем цвет и название из ColorScales
        private void colorScalesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            Color[] colors = colorScalesList.SelectedValue;

            pictureBox_Color1.BackColor = colors[0];
            pictureBox_Color2.BackColor = colors[1];
            pictureBox_Color3.BackColor = colors[2];
            pictureBox_Color4.BackColor = colors[3];
            pictureBox_Color5.BackColor = colors[4];
            pictureBox_Color6.BackColor = colors[5];

            textBox.Text = colorScalesList.SelectedText;
            ID_ColorScale = colorScalesList.SelectedID;
            ID_Colors = colorScalesList.SelectedItem.ID_Colors;

            buttonSave.Enabled = true;
        }

        private void colorScalesSave_Click(object sender, EventArgs e)
        {
            try
            {
                ESDC_DataBaseDataSet.ColorScalesRow newRowScale = eSDC_DataBaseDataSet.ColorScales.NewColorScalesRow();
                newRowScale.Name = textBox.Text;
                
                ESDC_DataBaseDataSet.ColorsRow newRowColors1 = eSDC_DataBaseDataSet.Colors.NewColorsRow();
                newRowColors1.Red = pictureBox_Color1.BackColor.R;
                newRowColors1.Green = pictureBox_Color1.BackColor.G;
                newRowColors1.Blue = pictureBox_Color1.BackColor.B;
                newRowColors1.Alpha = 255;
                ESDC_DataBaseDataSet.ColorsRow newRowColors2 = eSDC_DataBaseDataSet.Colors.NewColorsRow();
                newRowColors2.Red = pictureBox_Color2.BackColor.R;
                newRowColors2.Green = pictureBox_Color2.BackColor.G;
                newRowColors2.Blue = pictureBox_Color2.BackColor.B;
                newRowColors2.Alpha = 255;
                ESDC_DataBaseDataSet.ColorsRow newRowColors3 = eSDC_DataBaseDataSet.Colors.NewColorsRow();
                newRowColors3.Red = pictureBox_Color3.BackColor.R;
                newRowColors3.Green = pictureBox_Color3.BackColor.G;
                newRowColors3.Blue = pictureBox_Color3.BackColor.B;
                newRowColors3.Alpha = 255;
                ESDC_DataBaseDataSet.ColorsRow newRowColors4 = eSDC_DataBaseDataSet.Colors.NewColorsRow();
                newRowColors4.Red = pictureBox_Color4.BackColor.R;
                newRowColors4.Green = pictureBox_Color4.BackColor.G;
                newRowColors4.Blue = pictureBox_Color4.BackColor.B;
                newRowColors4.Alpha = 255;
                ESDC_DataBaseDataSet.ColorsRow newRowColors5 = eSDC_DataBaseDataSet.Colors.NewColorsRow();
                newRowColors5.Red = pictureBox_Color5.BackColor.R;
                newRowColors5.Green = pictureBox_Color5.BackColor.G;
                newRowColors5.Blue = pictureBox_Color5.BackColor.B;
                newRowColors5.Alpha = 255;
                ESDC_DataBaseDataSet.ColorsRow newRowColors6 = eSDC_DataBaseDataSet.Colors.NewColorsRow();
                newRowColors6.Red = pictureBox_Color6.BackColor.R;
                newRowColors6.Green = pictureBox_Color6.BackColor.G;
                newRowColors6.Blue = pictureBox_Color6.BackColor.B;
                newRowColors6.Alpha = 255;

                //Если не редактирование, то добавляем новую строку, иначе ищем строку по ID и меняем в неё поля
                if (ID_ColorScale == -1)
                {
                    eSDC_DataBaseDataSet.ColorScales.Rows.Add(newRowScale);
                    eSDC_DataBaseDataSet.Colors.Rows.Add(newRowColors1);
                    eSDC_DataBaseDataSet.Colors.Rows.Add(newRowColors2);
                    eSDC_DataBaseDataSet.Colors.Rows.Add(newRowColors3);
                    eSDC_DataBaseDataSet.Colors.Rows.Add(newRowColors4);
                    eSDC_DataBaseDataSet.Colors.Rows.Add(newRowColors5);
                    eSDC_DataBaseDataSet.Colors.Rows.Add(newRowColors6);
                }
                else
                {
                    Console.WriteLine("sadfsad");
                    newRowScale.ID_ColorScale = ID_ColorScale;
                    newRowColors1.ID_ColorScale = ID_Colors[0];
                    newRowColors2.ID_ColorScale = ID_Colors[1];
                    newRowColors3.ID_ColorScale = ID_Colors[2];
                    newRowColors4.ID_ColorScale = ID_Colors[3];
                    newRowColors5.ID_ColorScale = ID_Colors[4];
                    newRowColors6.ID_ColorScale = ID_Colors[5];

                    ESDC_DataBaseDataSet.ColorScalesRow editRow = eSDC_DataBaseDataSet.ColorScales.FindByID_ColorScale(ID_ColorScale);
                    editRow = newRowScale;

                    ESDC_DataBaseDataSet.ColorsRow editRowColor1 = eSDC_DataBaseDataSet.Colors.FindByID_Color(newRowColors1.ID_Color);
                    editRowColor1 = newRowColors1;
                    ESDC_DataBaseDataSet.ColorsRow editRowColor2 = eSDC_DataBaseDataSet.Colors.FindByID_Color(newRowColors2.ID_Color);
                    editRowColor2 = newRowColors2;
                    ESDC_DataBaseDataSet.ColorsRow editRowColor3 = eSDC_DataBaseDataSet.Colors.FindByID_Color(newRowColors3.ID_Color);
                    editRowColor3 = newRowColors3;
                    ESDC_DataBaseDataSet.ColorsRow editRowColor4 = eSDC_DataBaseDataSet.Colors.FindByID_Color(newRowColors4.ID_Color);
                    editRowColor4 = newRowColors4;
                    ESDC_DataBaseDataSet.ColorsRow editRowColor5 = eSDC_DataBaseDataSet.Colors.FindByID_Color(newRowColors5.ID_Color);
                    editRowColor5 = newRowColors5;
                    ESDC_DataBaseDataSet.ColorsRow editRowColor6 = eSDC_DataBaseDataSet.Colors.FindByID_Color(newRowColors6.ID_Color);
                    editRowColor6 = newRowColors6;
                }


                this.Validate();
                this.colorsBindingSource.EndEdit();
                this.colorScalesBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.eSDC_DataBaseDataSet);
            
            }
            catch
            {
                MessageBox.Show("Ошибка записи данных!");
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            ID_ColorScale = -1;
            colorScalesSave_Click(sender, e);
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBox.Text = null;
            numericUpDownColorR.Value = 255;
            numericUpDownColorG.Value = 255;
            numericUpDownColorB.Value = 255;
            //!!!!!!!!!!!!!
        }

        private void Panel_EditorColorScales_Load(object sender, EventArgs e)
        {
            this.colorsTableAdapter.Fill(this.eSDC_DataBaseDataSet.Colors);
            this.colorScalesTableAdapter.Fill(this.eSDC_DataBaseDataSet.ColorScales);
        }  
    }
}
