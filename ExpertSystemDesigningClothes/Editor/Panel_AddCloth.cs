﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemDesigningClothes
{
    /// <summary>
    /// Панель, для добавления тканей в БД 
    /// </summary>
    public partial class Panel_AddCloth : UserControl
    {
        public Panel_AddCloth()
        {
            InitializeComponent();
        }
        
        private void PanelEditor_AddCloth_Load(object sender, EventArgs e)
        {
            
        }

        private void colorScales_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void trackBarColorR_Scroll(object sender, EventArgs e)
        {

        }

        private void pictureBoxColor_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void pictureBoxColor_MouseUp(object sender, MouseEventArgs e)
        {

        }

        private void numericUpDownColorB_ValueChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDownColorR_ValueChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDownColorG_ValueChanged(object sender, EventArgs e)
        {

        }

        private void trackBarColorB_Scroll(object sender, EventArgs e)
        {

        }

        private void trackBarColorG_Scroll(object sender, EventArgs e)
        {

        }
    }
}
