﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemDesigningClothes
{
    /// <summary>
    /// Панель, для отображения списка элементов (тканей, цветов) 
    /// </summary>
    public partial class Panel_ListElements : UserControl
    {
        FormEditor editor;

        public Panel_ListElements(FormEditor editor)
        {
            InitializeComponent();
            this.editor = editor;
        }

        private void PanelEditor_List_Load(object sender, EventArgs e)
        {
            this.elementsTableAdapter.Fill(this.eSDC_DataBaseDataSet.Elements);
        }

        private void elementsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.elementsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.eSDC_DataBaseDataSet);

        }
        //Обновляем источник данных, после добавления нового элемента
        public void DataUpdate()
        {
            this.elementsTableAdapter.Fill(this.eSDC_DataBaseDataSet.Elements);
        }
        //Добавить новый элемент
        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            editor.typePanel("AddElement");
        }
        //Изменить выбранный элемент
        private void bindingNavigatorChangeItem_Click(object sender, EventArgs e)
        {
            int editID_Element = (int)elementsDataGridView.CurrentRow.Cells[0].Value;
            editor.setID_Element(editID_Element);
            editor.typePanel("EditElement");
        }
    }
}
