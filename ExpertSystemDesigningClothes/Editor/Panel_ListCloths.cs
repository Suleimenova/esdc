﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemDesigningClothes.Editor
{
    public partial class Panel_ListCloths : UserControl
    {
        public Panel_ListCloths(FormEditor editor)
        {
            InitializeComponent();
        }

        private void clothsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.clothsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.eSDC_DataBaseDataSet);

        }
        
        //Обновляем источник данных, после добавления нового элемента
        public void DataUpdate()
        {
            this.clothsTableAdapter.Fill(this.eSDC_DataBaseDataSet.Cloths);
        }
    }
}
