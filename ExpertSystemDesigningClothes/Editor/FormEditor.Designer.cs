﻿namespace ExpertSystemDesigningClothes
{
    partial class FormEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Добавить/Изменить");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Список элементов");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Редактор элементов", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Добавить/Изменить");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Редактор цветовых гамм ", new System.Windows.Forms.TreeNode[] {
            treeNode4});
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Добавить/Изменить");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Список тканей");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Редактор тканей", new System.Windows.Forms.TreeNode[] {
            treeNode6,
            treeNode7});
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Добавить/Изменить");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Список ограничений");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Редактор ограничений", new System.Windows.Forms.TreeNode[] {
            treeNode9,
            treeNode10});
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("Редакторы", new System.Windows.Forms.TreeNode[] {
            treeNode3,
            treeNode5,
            treeNode8,
            treeNode11});
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("Настройки");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEditor));
            this.treeView = new System.Windows.Forms.TreeView();
            this.panel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // treeView
            // 
            this.treeView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeView.Location = new System.Drawing.Point(12, 12);
            this.treeView.Name = "treeView";
            treeNode1.Name = "AddElement";
            treeNode1.Text = "Добавить/Изменить";
            treeNode2.Name = "ListElements";
            treeNode2.Text = "Список элементов";
            treeNode3.Name = "rootOne";
            treeNode3.Text = "Редактор элементов";
            treeNode4.Name = "EditColorScales";
            treeNode4.Text = "Добавить/Изменить";
            treeNode5.Name = "rootFour";
            treeNode5.Text = "Редактор цветовых гамм ";
            treeNode6.Name = "AddCloth";
            treeNode6.Text = "Добавить/Изменить";
            treeNode7.Name = "ListCloths";
            treeNode7.Text = "Список тканей";
            treeNode8.Name = "rootTwo";
            treeNode8.Text = "Редактор тканей";
            treeNode9.Name = "AddConstraint";
            treeNode9.Text = "Добавить/Изменить";
            treeNode10.Name = "ListConstraints";
            treeNode10.Text = "Список ограничений";
            treeNode11.Name = "rootThree";
            treeNode11.Text = "Редактор ограничений";
            treeNode12.Name = "rootMain";
            treeNode12.Text = "Редакторы";
            treeNode13.Name = "rootSettings";
            treeNode13.Text = "Настройки";
            this.treeView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode12,
            treeNode13});
            this.treeView.ShowRootLines = false;
            this.treeView.Size = new System.Drawing.Size(181, 346);
            this.treeView.TabIndex = 0;
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
            // 
            // panel
            // 
            this.panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel.BackColor = System.Drawing.SystemColors.Window;
            this.panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel.Location = new System.Drawing.Point(199, 12);
            this.panel.Margin = new System.Windows.Forms.Padding(0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(463, 346);
            this.panel.TabIndex = 1;
            // 
            // FormEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 371);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.treeView);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(690, 410);
            this.MinimumSize = new System.Drawing.Size(690, 410);
            this.Name = "FormEditor";
            this.Text = "Редактор";
            this.SizeChanged += new System.EventHandler(this.EditorForm_SizeChanged);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel;
        public System.Windows.Forms.TreeView treeView;
    }
}