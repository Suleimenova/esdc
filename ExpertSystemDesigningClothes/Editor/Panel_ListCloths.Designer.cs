﻿namespace ExpertSystemDesigningClothes.Editor
{
    partial class Panel_ListCloths
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Panel_ListCloths));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.eSDC_DataBaseDataSet = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSet();
            this.clothsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.clothsTableAdapter = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.ClothsTableAdapter();
            this.tableAdapterManager = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.TableAdapterManager();
            this.clothsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.clothsBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.clothsDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.eSDC_DataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clothsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clothsBindingNavigator)).BeginInit();
            this.clothsBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clothsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // eSDC_DataBaseDataSet
            // 
            this.eSDC_DataBaseDataSet.DataSetName = "ESDC_DataBaseDataSet";
            this.eSDC_DataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // clothsBindingSource
            // 
            this.clothsBindingSource.DataMember = "Cloths";
            this.clothsBindingSource.DataSource = this.eSDC_DataBaseDataSet;
            // 
            // clothsTableAdapter
            // 
            this.clothsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ClothsTableAdapter = this.clothsTableAdapter;
            this.tableAdapterManager.ColorScalesTableAdapter = null;
            this.tableAdapterManager.ColorsTableAdapter = null;
            this.tableAdapterManager.ConstraintsTableAdapter = null;
            this.tableAdapterManager.ElementsTableAdapter = null;
            this.tableAdapterManager.GroupsTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // clothsBindingNavigator
            // 
            this.clothsBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.clothsBindingNavigator.BindingSource = this.clothsBindingSource;
            this.clothsBindingNavigator.CountItem = null;
            this.clothsBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.clothsBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.clothsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.clothsBindingNavigatorSaveItem});
            this.clothsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.clothsBindingNavigator.MoveFirstItem = null;
            this.clothsBindingNavigator.MoveLastItem = null;
            this.clothsBindingNavigator.MoveNextItem = null;
            this.clothsBindingNavigator.MovePreviousItem = null;
            this.clothsBindingNavigator.Name = "clothsBindingNavigator";
            this.clothsBindingNavigator.PositionItem = null;
            this.clothsBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.clothsBindingNavigator.Size = new System.Drawing.Size(463, 25);
            this.clothsBindingNavigator.TabIndex = 0;
            this.clothsBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.Image = global::ExpertSystemDesigningClothes.Properties.Resources.plus;
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(79, 22);
            this.bindingNavigatorAddNewItem.Text = "Добавить";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.Image = global::ExpertSystemDesigningClothes.Properties.Resources.close;
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(71, 22);
            this.bindingNavigatorDeleteItem.Text = "Удалить";
            // 
            // clothsBindingNavigatorSaveItem
            // 
            this.clothsBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("clothsBindingNavigatorSaveItem.Image")));
            this.clothsBindingNavigatorSaveItem.Name = "clothsBindingNavigatorSaveItem";
            this.clothsBindingNavigatorSaveItem.Size = new System.Drawing.Size(85, 22);
            this.clothsBindingNavigatorSaveItem.Text = "Сохранить";
            this.clothsBindingNavigatorSaveItem.Click += new System.EventHandler(this.clothsBindingNavigatorSaveItem_Click);
            // 
            // clothsDataGridView
            // 
            this.clothsDataGridView.AllowUserToAddRows = false;
            this.clothsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.clothsDataGridView.AutoGenerateColumns = false;
            this.clothsDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.clothsDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.clothsDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.clothsDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.clothsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.clothsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewImageColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.clothsDataGridView.DataSource = this.clothsBindingSource;
            this.clothsDataGridView.Location = new System.Drawing.Point(6, 25);
            this.clothsDataGridView.Margin = new System.Windows.Forms.Padding(6, 0, 6, 6);
            this.clothsDataGridView.Name = "clothsDataGridView";
            this.clothsDataGridView.RowHeadersVisible = false;
            this.clothsDataGridView.Size = new System.Drawing.Size(451, 315);
            this.clothsDataGridView.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id_Cloth";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id_Cloth";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.DataPropertyName = "Picture";
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.MinimumWidth = 23;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Width = 23;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn2.HeaderText = "Название";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 259;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ID_Group";
            this.dataGridViewTextBoxColumn3.HeaderText = "ID_Группы";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 70;
            // 
            // Panel_ListCloths
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.clothsDataGridView);
            this.Controls.Add(this.clothsBindingNavigator);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MinimumSize = new System.Drawing.Size(463, 346);
            this.Name = "Panel_ListCloths";
            this.Size = new System.Drawing.Size(463, 346);
            ((System.ComponentModel.ISupportInitialize)(this.eSDC_DataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clothsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clothsBindingNavigator)).EndInit();
            this.clothsBindingNavigator.ResumeLayout(false);
            this.clothsBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clothsDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ESDC_DataBaseDataSet eSDC_DataBaseDataSet;
        private System.Windows.Forms.BindingSource clothsBindingSource;
        private ESDC_DataBaseDataSetTableAdapters.ClothsTableAdapter clothsTableAdapter;
        private ESDC_DataBaseDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator clothsBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton clothsBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView clothsDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;

    }
}
