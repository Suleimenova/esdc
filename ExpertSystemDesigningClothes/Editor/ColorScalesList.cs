﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemDesigningClothes.Editor
{
    public partial class ColorScalesList : ComboBox
    {
        public ColorScalesList()
        {
            InitializeComponent();

            DropDownStyle = ComboBoxStyle.DropDownList;
            DrawMode = DrawMode.OwnerDrawFixed;
            DrawItem += OnDrawItem;
        }

        // Data for each color in the list
        public class ColorInfo
        {
            public int ID { get; set; }
            public string Text { get; set; }
            public List<int> ID_Colors { get; set; }
            public Color Color1 { get; set; }
            public Color Color2 { get; set; }
            public Color Color3 { get; set; }
            public Color Color4 { get; set; }
            public Color Color5 { get; set; }
            public Color Color6 { get; set; }

            public ColorInfo(int id, string text, List<int> id_Colors, Color color1, Color color2, Color color3, Color color4, Color color5, Color color6)
            {
                ID = id;
                Text = text;
                ID_Colors = id_Colors;
                Color1 = color1;
                Color2 = color2;
                Color3 = color3;
                Color4 = color4;
                Color5 = color5;
                Color6 = color6;

            }
        }

        // Populate control with standard colors
        public void AddStandardColors()
        {
            // http://lookcolor.ru/cvet-v-odezhde/cvetovaya-gamma-v-odezhde/
            Items.Clear();
            //Items.Add(new ColorInfo(-1, "Деловой стиль 1", Color.FromArgb(236, 235, 229), Color.FromArgb(188, 200, 213), Color.FromArgb(167, 164, 164), Color.FromArgb(220, 196, 210), Color.FromArgb(149, 128, 123), Color.FromArgb(75, 53, 43)));
            //Items.Add(new ColorInfo(-1, "Деловой стиль 2", Color.FromArgb(230, 228, 229), Color.FromArgb(105, 136, 154), Color.FromArgb(53, 63, 91), Color.FromArgb(78, 64, 54), Color.FromArgb(106, 28, 41), Color.FromArgb(0, 0, 0)));
            //Items.Add(new ColorInfo(-1, "Деловой стиль 3", Color.FromArgb(229, 223, 209), Color.FromArgb(37, 128, 123), Color.FromArgb(48, 62, 51), Color.FromArgb(174, 161, 144), Color.FromArgb(90, 114, 136), Color.FromArgb(48, 38, 37)));

            //Items.Add(new ColorInfo(-1, "Классический стиль 1", Color.FromArgb(236, 235, 229), Color.FromArgb(238, 218, 207), Color.FromArgb(220, 202, 181), Color.FromArgb(220, 196, 210), Color.FromArgb(188, 200, 213), Color.FromArgb(134, 132, 159)));
            //Items.Add(new ColorInfo(-1, "Классический стиль 2", Color.FromArgb(255, 255, 255), Color.FromArgb(144, 116, 87), Color.FromArgb(255, 185, 156), Color.FromArgb(115, 78, 116), Color.FromArgb(124, 6, 12), Color.FromArgb(0, 0, 0)));
            //Items.Add(new ColorInfo(-1, "Классический стиль 3", Color.FromArgb(201, 189, 167), Color.FromArgb(208, 158, 50), Color.FromArgb(48, 93, 52), Color.FromArgb(34, 42, 62), Color.FromArgb(130, 47, 34), Color.FromArgb(48, 38, 37)));

            //Items.Add(new ColorInfo(-1, "Вечерний стиль 1", Color.FromArgb(205, 175, 104), Color.FromArgb(255, 198, 221), Color.FromArgb(23, 202, 181), Color.FromArgb(218, 62, 120), Color.FromArgb(231, 40, 49), Color.FromArgb(77, 87, 63)));
            //Items.Add(new ColorInfo(-1, "Вечерний стиль 2", Color.FromArgb(255, 255, 255), Color.FromArgb(205, 179, 147), Color.FromArgb(64, 69, 123), Color.FromArgb(183, 13, 51), Color.FromArgb(119, 157, 180), Color.FromArgb(0, 0, 0)));
            //Items.Add(new ColorInfo(-1, "Вечерний стиль 3", Color.FromArgb(244, 226, 187), Color.FromArgb(225, 170, 57), Color.FromArgb(42, 72, 169), Color.FromArgb(72, 47, 103), Color.FromArgb(211, 171, 179), Color.FromArgb(0, 0, 0)));

            //Items.Add(new ColorInfo(-1, "Романтический стиль 1", Color.FromArgb(252, 246, 231), Color.FromArgb(254, 227, 160), Color.FromArgb(210, 233, 229), Color.FromArgb(208, 179, 210), Color.FromArgb(188, 200, 213), Color.FromArgb(73, 163, 195)));
            //Items.Add(new ColorInfo(-1, "Романтический стиль 2", Color.FromArgb(234, 231, 222), Color.FromArgb(182, 139, 105), Color.FromArgb(30, 160, 141), Color.FromArgb(238, 202, 202), Color.FromArgb(119, 151, 180), Color.FromArgb(253, 117, 104)));
            //Items.Add(new ColorInfo(-1, "Романтический стиль 3", Color.FromArgb(192, 46, 35), Color.FromArgb(254, 229, 144), Color.FromArgb(211, 235, 221), Color.FromArgb(205, 161, 86), Color.FromArgb(205, 75, 75), Color.FromArgb(236, 227, 212)));

            //Items.Add(new ColorInfo(-1, "Повседневный стиль 1", Color.FromArgb(252, 246, 231), Color.FromArgb(202, 111, 81), Color.FromArgb(209, 202, 236), Color.FromArgb(208, 179, 210), Color.FromArgb(72, 96, 141), Color.FromArgb(101, 105, 117)));
            //Items.Add(new ColorInfo(-1, "Повседневный стиль 2", Color.FromArgb(162, 142, 131), Color.FromArgb(172, 172, 172), Color.FromArgb(200, 196, 185), Color.FromArgb(193, 143, 136), Color.FromArgb(128, 73, 43), Color.FromArgb(80, 57, 51)));
            //Items.Add(new ColorInfo(-1, "Повседневный стиль 3", Color.FromArgb(233, 205, 210), Color.FromArgb(148, 182, 160), Color.FromArgb(45, 52, 71), Color.FromArgb(216, 180, 153), Color.FromArgb(182, 187, 193), Color.FromArgb(26, 22, 27)));

            //Items.Add(new ColorInfo(-1, "Творческий стиль 1", Color.FromArgb(236, 237, 242), Color.FromArgb(244, 111, 81), Color.FromArgb(120, 202, 236), Color.FromArgb(94, 2, 81), Color.FromArgb(215, 135, 50), Color.FromArgb(62, 35, 17)));
            //Items.Add(new ColorInfo(-1, "Творческий стиль 2", Color.FromArgb(152, 197, 130), Color.FromArgb(85, 71, 121), Color.FromArgb(166, 149, 208), Color.FromArgb(9, 182, 199), Color.FromArgb(128, 73, 43), Color.FromArgb(0, 0, 0)));
            //Items.Add(new ColorInfo(-1, "Творческий стиль 3", Color.FromArgb(254, 247, 29), Color.FromArgb(108, 53, 108), Color.FromArgb(47, 53, 141), Color.FromArgb(188, 57, 140), Color.FromArgb(11, 180, 200), Color.FromArgb(240, 102, 18)));
        }

        //
        public void AddDBColors(ESDC_DataBaseDataSetTableAdapters.ColorsTableAdapter colorsTableAdapter, 
            ESDC_DataBaseDataSetTableAdapters.ColorScalesTableAdapter colorScalesTableAdapter)
        {
            ESDC_DataBaseDataSet.ColorScalesDataTable tableScales = new ESDC_DataBaseDataSet.ColorScalesDataTable();
            colorScalesTableAdapter.Fill(tableScales);

            ESDC_DataBaseDataSet.ColorsDataTable tableColors = new ESDC_DataBaseDataSet.ColorsDataTable();
            colorsTableAdapter.Fill(tableColors);

            List<Color> colors = new List<Color>();
            List<int> id_Colors = new List<int>();

            foreach (DataRow rowScale in tableScales.Rows)
            {
                foreach (DataRow rowColor in tableColors.Rows)
                {
                    if ((int)rowScale["ID_ColorScale"] == (int)rowColor["ID_ColorScale"])
                    {
                        id_Colors.Add((int)rowColor["ID_Color"]);
                        colors.Add(Color.FromArgb(
                            (int)rowColor["Red"], 
                            (int)rowColor["Green"], 
                            (int)rowColor["Blue"]));
                    }
                }

                Items.Add(
                    new ColorInfo(
                        (int)rowScale["ID_ColorScale"], 
                        rowScale["Name"].ToString(),
                        id_Colors,
                        colors[0],
                        colors[1],
                        colors[2],
                        colors[3],
                        colors[4],
                        colors[5]
                    )
                );
            }
        }


        // Draw list item
        protected void OnDrawItem(object sender, DrawItemEventArgs e)
        {
            int width = 24;     //Ширина прямоугольника с цветом
            int distance = 28;  //Растояние между прямоугольниками

            if (e.Index >= 0)
            {
                // Get this color
                ColorInfo color = (ColorInfo)Items[e.Index];

                // Fill background
                e.DrawBackground();

                // Draw color box
                Rectangle rect1 = new Rectangle();
                rect1.X = e.Bounds.X + 2;
                rect1.Y = e.Bounds.Y + 2;
                rect1.Width = width;
                rect1.Height = e.Bounds.Height - 5;
                e.Graphics.FillRectangle(new SolidBrush(color.Color1), rect1);
                e.Graphics.DrawRectangle(SystemPens.WindowText, rect1);


                Rectangle rect2 = new Rectangle();
                rect2.X = rect1.X + distance;
                rect2.Y = e.Bounds.Y + 2;
                rect2.Width = width;
                rect2.Height = e.Bounds.Height - 5;
                e.Graphics.FillRectangle(new SolidBrush(color.Color2), rect2);
                e.Graphics.DrawRectangle(SystemPens.WindowText, rect2);

                Rectangle rect3 = new Rectangle();
                rect3.X = rect2.X + distance;
                rect3.Y = e.Bounds.Y + 2;
                rect3.Width = width;
                rect3.Height = e.Bounds.Height - 5;
                e.Graphics.FillRectangle(new SolidBrush(color.Color3), rect3);
                e.Graphics.DrawRectangle(SystemPens.WindowText, rect3);

                Rectangle rect4 = new Rectangle();
                rect4.X = rect3.X + distance;
                rect4.Y = e.Bounds.Y + 2;
                rect4.Width = width;
                rect4.Height = e.Bounds.Height - 5;
                e.Graphics.FillRectangle(new SolidBrush(color.Color4), rect4);
                e.Graphics.DrawRectangle(SystemPens.WindowText, rect4);

                Rectangle rect5 = new Rectangle();
                rect5.X = rect4.X + distance;
                rect5.Y = e.Bounds.Y + 2;
                rect5.Width = width;
                rect5.Height = e.Bounds.Height - 5;
                e.Graphics.FillRectangle(new SolidBrush(color.Color5), rect5);
                e.Graphics.DrawRectangle(SystemPens.WindowText, rect5);

                Rectangle rect6 = new Rectangle();
                rect6.X = rect5.X + distance;
                rect6.Y = e.Bounds.Y + 2;
                rect6.Width = width;
                rect6.Height = e.Bounds.Height - 5;
                e.Graphics.FillRectangle(new SolidBrush(color.Color6), rect6);
                e.Graphics.DrawRectangle(SystemPens.WindowText, rect6);

                // Write color name
                Brush brush;
                if ((e.State & DrawItemState.Selected) != DrawItemState.None)
                    brush = SystemBrushes.HighlightText;
                else
                    brush = SystemBrushes.WindowText;
                e.Graphics.DrawString(color.Text, Font, brush,
                    e.Bounds.X + rect6.X + rect6.Width + 16,
                    e.Bounds.Y + ((e.Bounds.Height - Font.Height) / 2));

                // Draw the focus rectangle if appropriate
                if ((e.State & DrawItemState.NoFocusRect) == DrawItemState.None)
                    e.DrawFocusRectangle();
            }
        }

        /// <summary>
        /// Gets or sets the currently selected item.
        /// </summary>
        public new ColorInfo SelectedItem
        {
            get
            {
                return (ColorInfo)base.SelectedItem;
            }
            set
            {
                base.SelectedItem = value;
            }
        }

        /// <summary>
        /// Gets the text of the selected item, or sets the selection to
        /// the item with the specified text.
        /// </summary>
        public new string SelectedText
        {
            get
            {
                if (SelectedIndex >= 0)
                    return SelectedItem.Text;
                return String.Empty;
            }
            set
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    if (((ColorInfo)Items[i]).Text == value)
                    {
                        SelectedIndex = i;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the ID of the selected item, or sets the selection to
        /// the item with the specified text.
        /// </summary>
        public new int SelectedID
        {
            get
            {
                if (SelectedIndex >= 0)
                    return SelectedItem.ID;
                return -1;
            }
            set
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    if (((ColorInfo)Items[i]).ID == value)
                    {
                        SelectedIndex = i;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the value of the selected item, or sets the selection to
        /// the item with the specified value.
        /// </summary>
        public new Color[] SelectedValue
        {
            get
            {
                if (SelectedIndex >= 0)
                {
                    Color[] colors = new Color[6];
                    colors[0] = SelectedItem.Color1;
                    colors[1] = SelectedItem.Color2;
                    colors[2] = SelectedItem.Color3;
                    colors[3] = SelectedItem.Color4;
                    colors[4] = SelectedItem.Color5;
                    colors[5] = SelectedItem.Color6;

                    return colors;
                }
                return new Color[6];
            }
            set
            {
                //for (int i = 0; i < Items.Count; i++)
                //{
                //    if (((ColorInfo)Items[i]).Color1 == value)
                //    {
                //        SelectedIndex = i;
                //        break;
                //    }
                //}
            }
        }
    }
}
