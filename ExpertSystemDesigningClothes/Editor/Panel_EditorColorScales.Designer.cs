﻿namespace ExpertSystemDesigningClothes.Editor
{
    partial class Panel_EditorColorScales
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.trackBarColorR = new System.Windows.Forms.TrackBar();
            this.panel_Color1 = new System.Windows.Forms.Panel();
            this.pictureBox_Color1 = new System.Windows.Forms.PictureBox();
            this.numericUpDownColorB = new System.Windows.Forms.NumericUpDown();
            this.panel_Color2 = new System.Windows.Forms.Panel();
            this.pictureBox_Color2 = new System.Windows.Forms.PictureBox();
            this.panel_Color5 = new System.Windows.Forms.Panel();
            this.pictureBox_Color5 = new System.Windows.Forms.PictureBox();
            this.panelColor = new System.Windows.Forms.Panel();
            this.pictureBoxColor = new System.Windows.Forms.PictureBox();
            this.numericUpDownColorR = new System.Windows.Forms.NumericUpDown();
            this.panel_Color6 = new System.Windows.Forms.Panel();
            this.pictureBox_Color6 = new System.Windows.Forms.PictureBox();
            this.panel_Color3 = new System.Windows.Forms.Panel();
            this.pictureBox_Color3 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel_Color4 = new System.Windows.Forms.Panel();
            this.pictureBox_Color4 = new System.Windows.Forms.PictureBox();
            this.numericUpDownColorG = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.trackBarColorB = new System.Windows.Forms.TrackBar();
            this.trackBarColorG = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.eSDC_DataBaseDataSet = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSet();
            this.colorScalesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colorScalesTableAdapter = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.ColorScalesTableAdapter();
            this.tableAdapterManager = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.TableAdapterManager();
            this.colorsTableAdapter = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.ColorsTableAdapter();
            this.colorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.colorScalesList = new ExpertSystemDesigningClothes.Editor.ColorScalesList();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarColorR)).BeginInit();
            this.panel_Color1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColorB)).BeginInit();
            this.panel_Color2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color2)).BeginInit();
            this.panel_Color5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color5)).BeginInit();
            this.panelColor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColorR)).BeginInit();
            this.panel_Color6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color6)).BeginInit();
            this.panel_Color3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color3)).BeginInit();
            this.panel_Color4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColorG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarColorB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarColorG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eSDC_DataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorScalesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // trackBarColorR
            // 
            this.trackBarColorR.AutoSize = false;
            this.trackBarColorR.BackColor = System.Drawing.Color.IndianRed;
            this.trackBarColorR.Location = new System.Drawing.Point(211, 199);
            this.trackBarColorR.Margin = new System.Windows.Forms.Padding(0);
            this.trackBarColorR.Maximum = 255;
            this.trackBarColorR.Name = "trackBarColorR";
            this.trackBarColorR.Size = new System.Drawing.Size(131, 22);
            this.trackBarColorR.SmallChange = 5;
            this.trackBarColorR.TabIndex = 23;
            this.trackBarColorR.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarColorR.Value = 255;
            this.trackBarColorR.Scroll += new System.EventHandler(this.trackBarColorR_Scroll);
            // 
            // panel_Color1
            // 
            this.panel_Color1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Color1.Controls.Add(this.pictureBox_Color1);
            this.panel_Color1.Location = new System.Drawing.Point(68, 125);
            this.panel_Color1.Margin = new System.Windows.Forms.Padding(7);
            this.panel_Color1.Name = "panel_Color1";
            this.panel_Color1.Size = new System.Drawing.Size(42, 41);
            this.panel_Color1.TabIndex = 31;
            // 
            // pictureBox_Color1
            // 
            this.pictureBox_Color1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Color1.Location = new System.Drawing.Point(2, 1);
            this.pictureBox_Color1.Margin = new System.Windows.Forms.Padding(1);
            this.pictureBox_Color1.Name = "pictureBox_Color1";
            this.pictureBox_Color1.Size = new System.Drawing.Size(36, 37);
            this.pictureBox_Color1.TabIndex = 13;
            this.pictureBox_Color1.TabStop = false;
            this.pictureBox_Color1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseDown);
            this.pictureBox_Color1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseUp);
            // 
            // numericUpDownColorB
            // 
            this.numericUpDownColorB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericUpDownColorB.Location = new System.Drawing.Point(350, 253);
            this.numericUpDownColorB.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownColorB.Name = "numericUpDownColorB";
            this.numericUpDownColorB.Size = new System.Drawing.Size(49, 23);
            this.numericUpDownColorB.TabIndex = 22;
            this.numericUpDownColorB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownColorB.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownColorB.ValueChanged += new System.EventHandler(this.trackBarColorB_Scroll);
            // 
            // panel_Color2
            // 
            this.panel_Color2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Color2.Controls.Add(this.pictureBox_Color2);
            this.panel_Color2.Location = new System.Drawing.Point(126, 125);
            this.panel_Color2.Margin = new System.Windows.Forms.Padding(8);
            this.panel_Color2.Name = "panel_Color2";
            this.panel_Color2.Size = new System.Drawing.Size(42, 41);
            this.panel_Color2.TabIndex = 30;
            // 
            // pictureBox_Color2
            // 
            this.pictureBox_Color2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Color2.Location = new System.Drawing.Point(2, 1);
            this.pictureBox_Color2.Margin = new System.Windows.Forms.Padding(1);
            this.pictureBox_Color2.Name = "pictureBox_Color2";
            this.pictureBox_Color2.Size = new System.Drawing.Size(36, 37);
            this.pictureBox_Color2.TabIndex = 13;
            this.pictureBox_Color2.TabStop = false;
            this.pictureBox_Color2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseDown);
            this.pictureBox_Color2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseUp);
            // 
            // panel_Color5
            // 
            this.panel_Color5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Color5.Controls.Add(this.pictureBox_Color5);
            this.panel_Color5.Location = new System.Drawing.Point(299, 125);
            this.panel_Color5.Margin = new System.Windows.Forms.Padding(8);
            this.panel_Color5.Name = "panel_Color5";
            this.panel_Color5.Size = new System.Drawing.Size(42, 41);
            this.panel_Color5.TabIndex = 29;
            // 
            // pictureBox_Color5
            // 
            this.pictureBox_Color5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Color5.Location = new System.Drawing.Point(2, 1);
            this.pictureBox_Color5.Margin = new System.Windows.Forms.Padding(1);
            this.pictureBox_Color5.Name = "pictureBox_Color5";
            this.pictureBox_Color5.Size = new System.Drawing.Size(36, 37);
            this.pictureBox_Color5.TabIndex = 13;
            this.pictureBox_Color5.TabStop = false;
            this.pictureBox_Color5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseDown);
            this.pictureBox_Color5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseUp);
            // 
            // panelColor
            // 
            this.panelColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelColor.Controls.Add(this.pictureBoxColor);
            this.panelColor.Location = new System.Drawing.Point(68, 194);
            this.panelColor.MaximumSize = new System.Drawing.Size(82, 81);
            this.panelColor.MinimumSize = new System.Drawing.Size(82, 81);
            this.panelColor.Name = "panelColor";
            this.panelColor.Size = new System.Drawing.Size(82, 81);
            this.panelColor.TabIndex = 26;
            // 
            // pictureBoxColor
            // 
            this.pictureBoxColor.BackColor = System.Drawing.Color.White;
            this.pictureBoxColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxColor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxColor.Location = new System.Drawing.Point(4, 4);
            this.pictureBoxColor.Name = "pictureBoxColor";
            this.pictureBoxColor.Size = new System.Drawing.Size(74, 74);
            this.pictureBoxColor.TabIndex = 6;
            this.pictureBoxColor.TabStop = false;
            this.pictureBoxColor.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseDown);
            this.pictureBoxColor.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseUp);
            // 
            // numericUpDownColorR
            // 
            this.numericUpDownColorR.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericUpDownColorR.Location = new System.Drawing.Point(350, 198);
            this.numericUpDownColorR.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownColorR.Name = "numericUpDownColorR";
            this.numericUpDownColorR.Size = new System.Drawing.Size(49, 23);
            this.numericUpDownColorR.TabIndex = 20;
            this.numericUpDownColorR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownColorR.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownColorR.ValueChanged += new System.EventHandler(this.trackBarColorR_Scroll);
            // 
            // panel_Color6
            // 
            this.panel_Color6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Color6.Controls.Add(this.pictureBox_Color6);
            this.panel_Color6.Location = new System.Drawing.Point(357, 125);
            this.panel_Color6.Margin = new System.Windows.Forms.Padding(7);
            this.panel_Color6.Name = "panel_Color6";
            this.panel_Color6.Size = new System.Drawing.Size(42, 41);
            this.panel_Color6.TabIndex = 27;
            // 
            // pictureBox_Color6
            // 
            this.pictureBox_Color6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Color6.Location = new System.Drawing.Point(2, 1);
            this.pictureBox_Color6.Margin = new System.Windows.Forms.Padding(1);
            this.pictureBox_Color6.Name = "pictureBox_Color6";
            this.pictureBox_Color6.Size = new System.Drawing.Size(36, 37);
            this.pictureBox_Color6.TabIndex = 13;
            this.pictureBox_Color6.TabStop = false;
            this.pictureBox_Color6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseDown);
            this.pictureBox_Color6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseUp);
            // 
            // panel_Color3
            // 
            this.panel_Color3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Color3.Controls.Add(this.pictureBox_Color3);
            this.panel_Color3.Location = new System.Drawing.Point(184, 125);
            this.panel_Color3.Margin = new System.Windows.Forms.Padding(8);
            this.panel_Color3.Name = "panel_Color3";
            this.panel_Color3.Size = new System.Drawing.Size(42, 41);
            this.panel_Color3.TabIndex = 32;
            // 
            // pictureBox_Color3
            // 
            this.pictureBox_Color3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Color3.Location = new System.Drawing.Point(2, 1);
            this.pictureBox_Color3.Margin = new System.Windows.Forms.Padding(1);
            this.pictureBox_Color3.Name = "pictureBox_Color3";
            this.pictureBox_Color3.Size = new System.Drawing.Size(36, 37);
            this.pictureBox_Color3.TabIndex = 13;
            this.pictureBox_Color3.TabStop = false;
            this.pictureBox_Color3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseDown);
            this.pictureBox_Color3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(160, 199);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 15);
            this.label1.TabIndex = 17;
            this.label1.Text = "Red:";
            // 
            // panel_Color4
            // 
            this.panel_Color4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Color4.Controls.Add(this.pictureBox_Color4);
            this.panel_Color4.Location = new System.Drawing.Point(241, 125);
            this.panel_Color4.Margin = new System.Windows.Forms.Padding(8);
            this.panel_Color4.Name = "panel_Color4";
            this.panel_Color4.Size = new System.Drawing.Size(42, 41);
            this.panel_Color4.TabIndex = 28;
            // 
            // pictureBox_Color4
            // 
            this.pictureBox_Color4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Color4.Location = new System.Drawing.Point(2, 1);
            this.pictureBox_Color4.Margin = new System.Windows.Forms.Padding(1);
            this.pictureBox_Color4.Name = "pictureBox_Color4";
            this.pictureBox_Color4.Size = new System.Drawing.Size(36, 37);
            this.pictureBox_Color4.TabIndex = 13;
            this.pictureBox_Color4.TabStop = false;
            this.pictureBox_Color4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseDown);
            this.pictureBox_Color4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxColor_MouseUp);
            // 
            // numericUpDownColorG
            // 
            this.numericUpDownColorG.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericUpDownColorG.Location = new System.Drawing.Point(350, 225);
            this.numericUpDownColorG.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownColorG.Name = "numericUpDownColorG";
            this.numericUpDownColorG.Size = new System.Drawing.Size(49, 23);
            this.numericUpDownColorG.TabIndex = 21;
            this.numericUpDownColorG.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownColorG.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownColorG.ValueChanged += new System.EventHandler(this.trackBarColorG_Scroll);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(160, 227);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 15);
            this.label2.TabIndex = 18;
            this.label2.Text = "Green:";
            // 
            // trackBarColorB
            // 
            this.trackBarColorB.AutoSize = false;
            this.trackBarColorB.BackColor = System.Drawing.Color.LightSkyBlue;
            this.trackBarColorB.Location = new System.Drawing.Point(211, 254);
            this.trackBarColorB.Margin = new System.Windows.Forms.Padding(0);
            this.trackBarColorB.Maximum = 255;
            this.trackBarColorB.Name = "trackBarColorB";
            this.trackBarColorB.Size = new System.Drawing.Size(131, 22);
            this.trackBarColorB.SmallChange = 5;
            this.trackBarColorB.TabIndex = 25;
            this.trackBarColorB.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarColorB.Value = 255;
            this.trackBarColorB.Scroll += new System.EventHandler(this.trackBarColorB_Scroll);
            // 
            // trackBarColorG
            // 
            this.trackBarColorG.AutoSize = false;
            this.trackBarColorG.BackColor = System.Drawing.Color.LightGreen;
            this.trackBarColorG.Location = new System.Drawing.Point(211, 227);
            this.trackBarColorG.Margin = new System.Windows.Forms.Padding(0);
            this.trackBarColorG.Maximum = 255;
            this.trackBarColorG.Name = "trackBarColorG";
            this.trackBarColorG.Size = new System.Drawing.Size(131, 22);
            this.trackBarColorG.SmallChange = 5;
            this.trackBarColorG.TabIndex = 24;
            this.trackBarColorG.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarColorG.Value = 255;
            this.trackBarColorG.Scroll += new System.EventHandler(this.trackBarColorG_Scroll);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(160, 254);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 15);
            this.label3.TabIndex = 19;
            this.label3.Text = "Blue:";
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(189, 313);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(131, 27);
            this.buttonAdd.TabIndex = 34;
            this.buttonAdd.Text = "Добавить";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Enabled = false;
            this.buttonSave.Location = new System.Drawing.Point(326, 313);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(131, 27);
            this.buttonSave.TabIndex = 35;
            this.buttonSave.Text = "Сохранить";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.colorScalesSave_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(7, 314);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(131, 25);
            this.buttonClear.TabIndex = 36;
            this.buttonClear.Text = "Очистить";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // eSDC_DataBaseDataSet
            // 
            this.eSDC_DataBaseDataSet.DataSetName = "ESDC_DataBaseDataSet";
            this.eSDC_DataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // colorScalesBindingSource
            // 
            this.colorScalesBindingSource.DataMember = "ColorScales";
            this.colorScalesBindingSource.DataSource = this.eSDC_DataBaseDataSet;
            // 
            // colorScalesTableAdapter
            // 
            this.colorScalesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ClothsTableAdapter = null;
            this.tableAdapterManager.ColorScalesTableAdapter = this.colorScalesTableAdapter;
            this.tableAdapterManager.ColorsTableAdapter = this.colorsTableAdapter;
            this.tableAdapterManager.ConstraintsTableAdapter = null;
            this.tableAdapterManager.ElementsTableAdapter = null;
            this.tableAdapterManager.GroupsTableAdapter = null;
            this.tableAdapterManager.HelpTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // colorsTableAdapter
            // 
            this.colorsTableAdapter.ClearBeforeFill = true;
            // 
            // colorsBindingSource
            // 
            this.colorsBindingSource.DataMember = "Colors";
            this.colorsBindingSource.DataSource = this.eSDC_DataBaseDataSet;
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(68, 91);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(331, 23);
            this.textBox.TabIndex = 38;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(65, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(157, 15);
            this.label4.TabIndex = 39;
            this.label4.Text = "Название цветовой гаммы:";
            // 
            // colorScalesList
            // 
            this.colorScalesList.BackColor = System.Drawing.Color.White;
            this.colorScalesList.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.colorScalesList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.colorScalesList.FormattingEnabled = true;
            this.colorScalesList.Location = new System.Drawing.Point(7, 3);
            this.colorScalesList.Name = "colorScalesList";
            this.colorScalesList.SelectedID = -1;
            this.colorScalesList.SelectedItem = null;
            this.colorScalesList.SelectedValue = new System.Drawing.Color[] {
        System.Drawing.Color.Empty,
        System.Drawing.Color.Empty,
        System.Drawing.Color.Empty,
        System.Drawing.Color.Empty,
        System.Drawing.Color.Empty,
        System.Drawing.Color.Empty};
            this.colorScalesList.Size = new System.Drawing.Size(450, 24);
            this.colorScalesList.TabIndex = 37;
            this.colorScalesList.SelectedIndexChanged += new System.EventHandler(this.colorScalesList_SelectedIndexChanged);
            // 
            // Panel_EditorColorScales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.colorScalesList);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.trackBarColorR);
            this.Controls.Add(this.panel_Color1);
            this.Controls.Add(this.numericUpDownColorB);
            this.Controls.Add(this.panel_Color2);
            this.Controls.Add(this.panel_Color5);
            this.Controls.Add(this.panelColor);
            this.Controls.Add(this.numericUpDownColorR);
            this.Controls.Add(this.panel_Color6);
            this.Controls.Add(this.panel_Color3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel_Color4);
            this.Controls.Add(this.numericUpDownColorG);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.trackBarColorB);
            this.Controls.Add(this.trackBarColorG);
            this.Controls.Add(this.label3);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MinimumSize = new System.Drawing.Size(463, 346);
            this.Name = "Panel_EditorColorScales";
            this.Size = new System.Drawing.Size(463, 346);
            this.Load += new System.EventHandler(this.Panel_EditorColorScales_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarColorR)).EndInit();
            this.panel_Color1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColorB)).EndInit();
            this.panel_Color2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color2)).EndInit();
            this.panel_Color5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color5)).EndInit();
            this.panelColor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColorR)).EndInit();
            this.panel_Color6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color6)).EndInit();
            this.panel_Color3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color3)).EndInit();
            this.panel_Color4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Color4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColorG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarColorB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarColorG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eSDC_DataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorScalesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar trackBarColorR;
        private System.Windows.Forms.Panel panel_Color1;
        private System.Windows.Forms.PictureBox pictureBox_Color1;
        private System.Windows.Forms.NumericUpDown numericUpDownColorB;
        private System.Windows.Forms.Panel panel_Color2;
        private System.Windows.Forms.PictureBox pictureBox_Color2;
        private System.Windows.Forms.Panel panel_Color5;
        private System.Windows.Forms.PictureBox pictureBox_Color5;
        private System.Windows.Forms.Panel panelColor;
        public System.Windows.Forms.PictureBox pictureBoxColor;
        private System.Windows.Forms.NumericUpDown numericUpDownColorR;
        private System.Windows.Forms.Panel panel_Color6;
        private System.Windows.Forms.PictureBox pictureBox_Color6;
        private System.Windows.Forms.Panel panel_Color3;
        private System.Windows.Forms.PictureBox pictureBox_Color3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel_Color4;
        private System.Windows.Forms.PictureBox pictureBox_Color4;
        private System.Windows.Forms.NumericUpDown numericUpDownColorG;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar trackBarColorB;
        private System.Windows.Forms.TrackBar trackBarColorG;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonClear;
        private ESDC_DataBaseDataSet eSDC_DataBaseDataSet;
        private System.Windows.Forms.BindingSource colorScalesBindingSource;
        private ESDC_DataBaseDataSetTableAdapters.ColorScalesTableAdapter colorScalesTableAdapter;
        private ESDC_DataBaseDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private ESDC_DataBaseDataSetTableAdapters.ColorsTableAdapter colorsTableAdapter;
        private System.Windows.Forms.BindingSource colorsBindingSource;
        private ColorScalesList colorScalesList;
        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.Label label4;
    }
}
