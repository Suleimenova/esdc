﻿namespace ExpertSystemDesigningClothes.Editor
{
    partial class Panel_ListConstraints
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Panel_ListConstraints));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.eSDC_DataBaseDataSet = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSet();
            this.constraintsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.constraintsTableAdapter = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.ConstraintsTableAdapter();
            this.tableAdapterManager = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.TableAdapterManager();
            this.constraintsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorChangeItem = new System.Windows.Forms.ToolStripButton();
            this.constraintsBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.constraintsDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.eSDC_DataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.constraintsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.constraintsBindingNavigator)).BeginInit();
            this.constraintsBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.constraintsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // eSDC_DataBaseDataSet
            // 
            this.eSDC_DataBaseDataSet.DataSetName = "ESDC_DataBaseDataSet";
            this.eSDC_DataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // constraintsBindingSource
            // 
            this.constraintsBindingSource.DataMember = "Constraints";
            this.constraintsBindingSource.DataSource = this.eSDC_DataBaseDataSet;
            // 
            // constraintsTableAdapter
            // 
            this.constraintsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ClothsTableAdapter = null;
            this.tableAdapterManager.ColorScalesTableAdapter = null;
            this.tableAdapterManager.ColorsTableAdapter = null;
            this.tableAdapterManager.ConstraintsTableAdapter = this.constraintsTableAdapter;
            this.tableAdapterManager.ElementsTableAdapter = null;
            this.tableAdapterManager.GroupsTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // constraintsBindingNavigator
            // 
            this.constraintsBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.constraintsBindingNavigator.BindingSource = this.constraintsBindingSource;
            this.constraintsBindingNavigator.CountItem = null;
            this.constraintsBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.constraintsBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.constraintsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorChangeItem,
            this.bindingNavigatorDeleteItem,
            this.constraintsBindingNavigatorSaveItem});
            this.constraintsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.constraintsBindingNavigator.MoveFirstItem = null;
            this.constraintsBindingNavigator.MoveLastItem = null;
            this.constraintsBindingNavigator.MoveNextItem = null;
            this.constraintsBindingNavigator.MovePreviousItem = null;
            this.constraintsBindingNavigator.Name = "constraintsBindingNavigator";
            this.constraintsBindingNavigator.PositionItem = null;
            this.constraintsBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.constraintsBindingNavigator.Size = new System.Drawing.Size(463, 25);
            this.constraintsBindingNavigator.TabIndex = 0;
            this.constraintsBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.Image = global::ExpertSystemDesigningClothes.Properties.Resources.plus;
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(79, 22);
            this.bindingNavigatorAddNewItem.Text = "Добавить";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.Image = global::ExpertSystemDesigningClothes.Properties.Resources.close;
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(71, 22);
            this.bindingNavigatorDeleteItem.Text = "Удалить";
            // 
            // bindingNavigatorChangeItem
            // 
            this.bindingNavigatorChangeItem.Image = global::ExpertSystemDesigningClothes.Properties.Resources.edit2;
            this.bindingNavigatorChangeItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bindingNavigatorChangeItem.Name = "bindingNavigatorChangeItem";
            this.bindingNavigatorChangeItem.Size = new System.Drawing.Size(81, 22);
            this.bindingNavigatorChangeItem.Text = "Изменить";
            this.bindingNavigatorChangeItem.Click += new System.EventHandler(this.bindingNavigatorChangeItem_Click);
            // 
            // constraintsBindingNavigatorSaveItem
            // 
            this.constraintsBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("constraintsBindingNavigatorSaveItem.Image")));
            this.constraintsBindingNavigatorSaveItem.Name = "constraintsBindingNavigatorSaveItem";
            this.constraintsBindingNavigatorSaveItem.Size = new System.Drawing.Size(85, 22);
            this.constraintsBindingNavigatorSaveItem.Text = "Сохранить";
            this.constraintsBindingNavigatorSaveItem.Click += new System.EventHandler(this.constraintsBindingNavigatorSaveItem_Click);
            // 
            // constraintsDataGridView
            // 
            this.constraintsDataGridView.AllowUserToAddRows = false;
            this.constraintsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.constraintsDataGridView.AutoGenerateColumns = false;
            this.constraintsDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.constraintsDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.constraintsDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.constraintsDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.constraintsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.constraintsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.constraintsDataGridView.DataSource = this.constraintsBindingSource;
            this.constraintsDataGridView.Location = new System.Drawing.Point(6, 25);
            this.constraintsDataGridView.Margin = new System.Windows.Forms.Padding(6, 0, 6, 6);
            this.constraintsDataGridView.Name = "constraintsDataGridView";
            this.constraintsDataGridView.RowHeadersVisible = false;
            this.constraintsDataGridView.Size = new System.Drawing.Size(451, 214);
            this.constraintsDataGridView.TabIndex = 1;
            this.constraintsDataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.constraintsDataGridView_CellMouseClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ID_Constraint";
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 32;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Constraint";
            this.dataGridViewTextBoxColumn2.HeaderText = "Ограничение";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 400;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Description";
            this.dataGridViewTextBoxColumn3.HeaderText = "Описание";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Visible = false;
            this.dataGridViewTextBoxColumn3.Width = 205;
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDescription.Location = new System.Drawing.Point(6, 248);
            this.textBoxDescription.Margin = new System.Windows.Forms.Padding(6);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(451, 92);
            this.textBoxDescription.TabIndex = 2;
            // 
            // Panel_ListConstraints
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.textBoxDescription);
            this.Controls.Add(this.constraintsDataGridView);
            this.Controls.Add(this.constraintsBindingNavigator);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MinimumSize = new System.Drawing.Size(463, 346);
            this.Name = "Panel_ListConstraints";
            this.Size = new System.Drawing.Size(463, 346);
            this.Load += new System.EventHandler(this.Panel_ListConstraints_Load);
            ((System.ComponentModel.ISupportInitialize)(this.eSDC_DataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.constraintsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.constraintsBindingNavigator)).EndInit();
            this.constraintsBindingNavigator.ResumeLayout(false);
            this.constraintsBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.constraintsDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ESDC_DataBaseDataSet eSDC_DataBaseDataSet;
        private System.Windows.Forms.BindingSource constraintsBindingSource;
        private ESDC_DataBaseDataSetTableAdapters.ConstraintsTableAdapter constraintsTableAdapter;
        private ESDC_DataBaseDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator constraintsBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton constraintsBindingNavigatorSaveItem;
        public System.Windows.Forms.DataGridView constraintsDataGridView;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.ToolStripButton bindingNavigatorChangeItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    }
}
