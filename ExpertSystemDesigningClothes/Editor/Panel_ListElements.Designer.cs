﻿namespace ExpertSystemDesigningClothes
{
    partial class Panel_ListElements
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Panel_ListElements));
            this.eSDC_DataBaseDataSet = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSet();
            this.elementsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.elementsTableAdapter = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.ElementsTableAdapter();
            this.tableAdapterManager = new ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.TableAdapterManager();
            this.elementsDataGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.Picture_Back = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.elementsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorChangeItem = new System.Windows.Forms.ToolStripButton();
            this.elementsBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.eSDC_DataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementsBindingNavigator)).BeginInit();
            this.elementsBindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // eSDC_DataBaseDataSet
            // 
            this.eSDC_DataBaseDataSet.DataSetName = "ESDC_DataBaseDataSet";
            this.eSDC_DataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // elementsBindingSource
            // 
            this.elementsBindingSource.DataMember = "Elements";
            this.elementsBindingSource.DataSource = this.eSDC_DataBaseDataSet;
            // 
            // elementsTableAdapter
            // 
            this.elementsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ClothsTableAdapter = null;
            this.tableAdapterManager.ColorScalesTableAdapter = null;
            this.tableAdapterManager.ColorsTableAdapter = null;
            this.tableAdapterManager.ConstraintsTableAdapter = null;
            this.tableAdapterManager.ElementsTableAdapter = this.elementsTableAdapter;
            this.tableAdapterManager.GroupsTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ExpertSystemDesigningClothes.ESDC_DataBaseDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // elementsDataGridView
            // 
            this.elementsDataGridView.AllowUserToAddRows = false;
            this.elementsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.elementsDataGridView.AutoGenerateColumns = false;
            this.elementsDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.elementsDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.elementsDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.elementsDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.elementsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.elementsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.dataGridViewImageColumn1,
            this.Picture_Back,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.elementsDataGridView.DataSource = this.elementsBindingSource;
            this.elementsDataGridView.Location = new System.Drawing.Point(6, 25);
            this.elementsDataGridView.Margin = new System.Windows.Forms.Padding(6, 0, 6, 6);
            this.elementsDataGridView.Name = "elementsDataGridView";
            this.elementsDataGridView.RowHeadersVisible = false;
            this.elementsDataGridView.Size = new System.Drawing.Size(451, 315);
            this.elementsDataGridView.TabIndex = 1;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "ID_Element";
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.DataPropertyName = "Picture";
            dataGridViewCellStyle7.NullValue = null;
            this.dataGridViewImageColumn1.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn1.MinimumWidth = 23;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.ReadOnly = true;
            this.dataGridViewImageColumn1.ToolTipText = "Изображение элемента одежды";
            this.dataGridViewImageColumn1.Width = 23;
            // 
            // Picture_Back
            // 
            this.Picture_Back.DataPropertyName = "Picture_Back";
            this.Picture_Back.HeaderText = "Picture_Back";
            this.Picture_Back.Name = "Picture_Back";
            this.Picture_Back.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn2.HeaderText = "Название";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 259;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ID_Group";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn3.HeaderText = "ID_Группы";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 70;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Location_X";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn4.HeaderText = " X";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 40;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Location_Y";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn5.HeaderText = " Y";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 40;
            // 
            // elementsBindingNavigator
            // 
            this.elementsBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.elementsBindingNavigator.BindingSource = this.elementsBindingSource;
            this.elementsBindingNavigator.CountItem = null;
            this.elementsBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.elementsBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.elementsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorChangeItem,
            this.bindingNavigatorDeleteItem,
            this.elementsBindingNavigatorSaveItem});
            this.elementsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.elementsBindingNavigator.MoveFirstItem = null;
            this.elementsBindingNavigator.MoveLastItem = null;
            this.elementsBindingNavigator.MoveNextItem = null;
            this.elementsBindingNavigator.MovePreviousItem = null;
            this.elementsBindingNavigator.Name = "elementsBindingNavigator";
            this.elementsBindingNavigator.PositionItem = null;
            this.elementsBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.elementsBindingNavigator.Size = new System.Drawing.Size(463, 25);
            this.elementsBindingNavigator.TabIndex = 0;
            this.elementsBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.Image = global::ExpertSystemDesigningClothes.Properties.Resources.plus;
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(79, 22);
            this.bindingNavigatorAddNewItem.Text = "Добавить";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.Image = global::ExpertSystemDesigningClothes.Properties.Resources.close;
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(71, 22);
            this.bindingNavigatorDeleteItem.Text = "Удалить";
            // 
            // bindingNavigatorChangeItem
            // 
            this.bindingNavigatorChangeItem.Image = global::ExpertSystemDesigningClothes.Properties.Resources.edit2;
            this.bindingNavigatorChangeItem.Name = "bindingNavigatorChangeItem";
            this.bindingNavigatorChangeItem.Size = new System.Drawing.Size(81, 22);
            this.bindingNavigatorChangeItem.Text = "Изменить";
            this.bindingNavigatorChangeItem.Click += new System.EventHandler(this.bindingNavigatorChangeItem_Click);
            // 
            // elementsBindingNavigatorSaveItem
            // 
            this.elementsBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("elementsBindingNavigatorSaveItem.Image")));
            this.elementsBindingNavigatorSaveItem.Name = "elementsBindingNavigatorSaveItem";
            this.elementsBindingNavigatorSaveItem.Size = new System.Drawing.Size(85, 22);
            this.elementsBindingNavigatorSaveItem.Text = "Сохранить";
            this.elementsBindingNavigatorSaveItem.Click += new System.EventHandler(this.elementsBindingNavigatorSaveItem_Click);
            // 
            // Panel_ListElements
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.elementsDataGridView);
            this.Controls.Add(this.elementsBindingNavigator);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MinimumSize = new System.Drawing.Size(463, 346);
            this.Name = "Panel_ListElements";
            this.Size = new System.Drawing.Size(463, 346);
            this.Load += new System.EventHandler(this.PanelEditor_List_Load);
            ((System.ComponentModel.ISupportInitialize)(this.eSDC_DataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementsBindingNavigator)).EndInit();
            this.elementsBindingNavigator.ResumeLayout(false);
            this.elementsBindingNavigator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ESDC_DataBaseDataSet eSDC_DataBaseDataSet;
        private System.Windows.Forms.BindingSource elementsBindingSource;
        private ESDC_DataBaseDataSetTableAdapters.ElementsTableAdapter elementsTableAdapter;
        private ESDC_DataBaseDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.BindingNavigator elementsBindingNavigator;
        private System.Windows.Forms.ToolStripButton elementsBindingNavigatorSaveItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorChangeItem;
        public System.Windows.Forms.DataGridView elementsDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn Picture_Back;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
















    }
}
