﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpertSystemDesigningClothes.Editor
{
    public partial class Panel_ListConstraints : UserControl
    {
        FormEditor editor;

        public Panel_ListConstraints(FormEditor editor)
        {
            InitializeComponent();
            this.editor = editor;
        }
        //Обновляем источник данных, после добавления нового элемента
        public void DataUpdate()
        {
            this.constraintsTableAdapter.Fill(this.eSDC_DataBaseDataSet.Constraints);
        }

        private void constraintsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.constraintsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.eSDC_DataBaseDataSet);

        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            editor.typePanel("AddConstraint");
        }

        private void bindingNavigatorChangeItem_Click(object sender, EventArgs e)
        {
            int editID_Constraint = (int)constraintsDataGridView.CurrentRow.Cells[0].Value;
            editor.setID_Constraint(editID_Constraint);
            editor.typePanel("EditConstraint");
        }

        //Загрузка описания
        private void constraintsDataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            textBoxDescription.Text = constraintsDataGridView[2,e.RowIndex].Value.ToString();
        }

        //Загрузка данных
        private void Panel_ListConstraints_Load(object sender, EventArgs e)
        {
            this.constraintsTableAdapter.Fill(this.eSDC_DataBaseDataSet.Constraints);
        }
    }
}
