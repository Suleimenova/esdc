﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemDesigningClothes.Utilities
{
    class FloodFillScanlineStack
    {
        public Bitmap bmp;
        Stack<Point> stack;

        public FloodFillScanlineStack(Bitmap bmp, Point point, Color color)
        {
            this.bmp = bmp;
            stack = new Stack<Point>();

            bmp.MakeTransparent();
            Fill(point, color, bmp.GetPixel(point.X, point.Y));
        }

        //Построчный алгоритм заливки
        public void Fill(Point point, Color newColor, Color oldColor)
        {
            if (newColor == oldColor) return; //Избегаем бесконечного цикла
            stack.Clear();

            stack.Push(point);

            int y1;
            bool spanLeft, spanRight;

            while (stack.Count != 0)
            {
                point = stack.Pop();
                y1 = point.Y;

                while (y1 >= 0 && bmp.GetPixel(point.X, y1) == oldColor)
                    y1--;

                y1++;
                spanLeft = false;
                spanRight = false;

                while (y1 < bmp.Height && bmp.GetPixel(point.X, y1) == oldColor)
                {
                    bmp.SetPixel(point.X, y1, newColor);
                    if (!spanLeft && point.X > 0 && bmp.GetPixel(point.X - 1, y1) == oldColor)
                    {
                        stack.Push(new Point(point.X - 1, y1));
                        spanLeft = true;
                    }
                    else if (spanLeft && point.X > 0 && bmp.GetPixel(point.X - 1, y1) != oldColor)
                    {
                        spanLeft = false;
                    }
                    if (!spanRight && point.X < bmp.Width - 1 && bmp.GetPixel(point.X + 1, y1) == oldColor)
                    {
                        stack.Push(new Point(point.X + 1, y1));
                        spanRight = true;
                    }
                    else if (spanRight && point.X < bmp.Width - 1 && bmp.GetPixel(point.X + 1, y1) != oldColor)
                    {
                        spanRight = false;
                    }
                    y1++;
                }
            }
        }
    }
}
